use arrow::array::{
    ArrayBuilder, Float32Builder, Float64Builder, Int16Builder, Int32Builder, Int64Builder,
    Int8Builder, UInt16Builder, UInt32Builder, UInt64Builder, UInt8Builder,
};
use arrow::datatypes::{
    ArrowPrimitiveType, Float32Type, Float64Type, Int16Type, Int32Type, Int64Type, Int8Type,
    UInt16Type, UInt32Type, UInt64Type, UInt8Type,
};

pub trait ToArrowType {
    type ArrowType: ArrowPrimitiveType;
    type ArrowBuilder: ArrayBuilder;
}

impl ToArrowType for u8 {
    type ArrowType = UInt8Type;
    type ArrowBuilder = UInt8Builder;
}

impl ToArrowType for u16 {
    type ArrowType = UInt16Type;
    type ArrowBuilder = UInt16Builder;
}

impl ToArrowType for u32 {
    type ArrowType = UInt32Type;
    type ArrowBuilder = UInt32Builder;
}

impl ToArrowType for u64 {
    type ArrowType = UInt64Type;
    type ArrowBuilder = UInt64Builder;
}

impl ToArrowType for i8 {
    type ArrowType = Int8Type;
    type ArrowBuilder = Int8Builder;
}

impl ToArrowType for i16 {
    type ArrowType = Int16Type;
    type ArrowBuilder = Int16Builder;
}

impl ToArrowType for i32 {
    type ArrowType = Int32Type;
    type ArrowBuilder = Int32Builder;
}

impl ToArrowType for i64 {
    type ArrowType = Int64Type;
    type ArrowBuilder = Int64Builder;
}

impl ToArrowType for f32 {
    type ArrowType = Float32Type;
    type ArrowBuilder = Float32Builder;
}

impl ToArrowType for f64 {
    type ArrowType = Float64Type;
    type ArrowBuilder = Float64Builder;
}
