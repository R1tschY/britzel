use std::sync::Arc;

use crate::array::{ToArray, ToArrayRef};
use crate::types::ToArrowType;
use crate::ArrowResult;
use arrow::array::{Array, ArrayData, ArrayRef, PrimitiveArray, UInt32Array};
use arrow::buffer::Buffer;
use arrow::datatypes::*;
use arrow::record_batch::RecordBatch;
use crate::data_size::DataSize;

pub fn record_batch_from_array<T: Array + 'static>(arr: T) -> RecordBatch {
    let array = Arc::new(arr);
    let schema = Schema::new(vec![Field::new("value", array.data_type().clone(), false)]);

    RecordBatch::try_new(Arc::new(schema), vec![array]).unwrap()
}

pub fn record_batch_from_slice<U: Array + 'static, T: ToArray<U>>(slice: T) -> RecordBatch {
    record_batch_from_array(slice.to_array())
}

pub struct ColumnIterator<'a>(usize, &'a RecordBatch);

impl<'a> Iterator for ColumnIterator<'a> {
    type Item = &'a ArrayRef;

    fn next(&mut self) -> Option<Self::Item> {
        if self.0 < self.1.num_columns() {
            let result = Some(self.1.column(self.0));
            self.0 += 1;
            result
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let remaining = self.1.num_columns() - self.0;
        (remaining, Some(remaining))
    }
}

pub trait RecordBatchFunctions {
    fn as_record_batch(&self) -> &RecordBatch;

    fn typed_column<T: ToArrowType>(
        &self,
        i: usize,
    ) -> &[<<T as ToArrowType>::ArrowType as ArrowPrimitiveType>::Native]
    where
        T::ArrowType: ArrowNumericType,
    {
        let arr = self
            .as_record_batch()
            .column(i)
            .as_any()
            .downcast_ref::<PrimitiveArray<T::ArrowType>>()
            .expect("wrong type");
        arr.value_slice(0, arr.len())
    }

    fn typed_value<T: ToArrowType>(
        &self,
        col: usize,
        row: usize,
    ) -> <<T as ToArrowType>::ArrowType as ArrowPrimitiveType>::Native
    where
        T::ArrowType: ArrowNumericType,
    {
        let arr = self
            .as_record_batch()
            .column(col)
            .as_any()
            .downcast_ref::<PrimitiveArray<T::ArrowType>>()
            .expect("wrong type");
        arr.value(row)
    }

    fn slice(&self, offset: usize, length: usize) -> RecordBatch {
        self.map_columns(|arr| arr.slice(offset, length)).unwrap()
    }

    fn map_columns(&self, f: impl Fn(&ArrayRef) -> ArrayRef) -> ArrowResult<RecordBatch> {
        RecordBatch::try_new(
            self.as_record_batch().schema().clone(),
            self.iter_columns().map(f).collect(),
        )
    }

    fn try_map_columns(
        &self,
        f: impl Fn(&ArrayRef) -> ArrowResult<ArrayRef>,
    ) -> ArrowResult<RecordBatch> {
        let columns: ArrowResult<Vec<ArrayRef>> = self.iter_columns().map(f).collect();
        RecordBatch::try_new(self.as_record_batch().schema().clone(), columns?)
    }

    fn take(&self, length: usize) -> RecordBatch {
        self.slice(0, length)
    }

    fn iter_columns(&self) -> ColumnIterator {
        ColumnIterator(0, self.as_record_batch())
    }
}

impl RecordBatchFunctions for RecordBatch {
    fn as_record_batch(&self) -> &RecordBatch {
        &self
    }
}
