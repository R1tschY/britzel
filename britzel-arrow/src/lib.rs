pub mod array;
pub mod compute;
pub mod record_batch;
pub mod types;
pub mod data_size;

pub type ArrowResult<T> = arrow::error::Result<T>;
pub type ArrowError = arrow::error::ArrowError;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
