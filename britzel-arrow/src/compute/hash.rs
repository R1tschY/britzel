use std::any::Any;
use std::collections::hash_map::DefaultHasher;
use std::hash::Hash;
use std::hash::Hasher;

use arrow::array::*;
use arrow::datatypes::DataType;
use arrow::error::ArrowError;
use ordered_float::OrderedFloat;

use crate::ArrowResult;

trait HashArray {
    fn hash_array(&self) -> ArrowResult<UInt64Array>;
}

macro_rules! impl_hash_array {
    ($array_type:ty) => {
        impl HashArray for $array_type {
            fn hash_array(&self) -> ArrowResult<UInt64Array> {
                let mut builder = UInt64Array::builder(self.len());
                for i in 0..self.len() {
                    let mut hasher = DefaultHasher::new();
                    let optional = if self.is_null(i) {
                        None
                    } else {
                        Some(self.value(i))
                    };
                    optional.hash(&mut hasher);
                    builder.append_value(hasher.finish())?;
                }
                Ok(builder.finish())
            }
        }
    };
}

impl_hash_array!(UInt8Array);
impl_hash_array!(UInt16Array);
impl_hash_array!(UInt32Array);
impl_hash_array!(UInt64Array);
impl_hash_array!(Int8Array);
impl_hash_array!(Int16Array);
impl_hash_array!(Int32Array);
impl_hash_array!(Int64Array);
impl_hash_array!(BooleanArray);

impl HashArray for Float32Array {
    fn hash_array(&self) -> ArrowResult<UInt64Array> {
        let mut builder = UInt64Array::builder(self.len());
        for i in 0..self.len() {
            let mut hasher = DefaultHasher::new();
            let optional: Option<OrderedFloat<f32>> = if self.is_null(i) {
                None
            } else {
                Some(self.value(i).into())
            };
            optional.hash(&mut hasher);
            builder.append_value(hasher.finish())?;
        }
        Ok(builder.finish())
    }
}

impl HashArray for Float64Array {
    fn hash_array(&self) -> ArrowResult<UInt64Array> {
        let mut builder = UInt64Array::builder(self.len());
        for i in 0..self.len() {
            let mut hasher = DefaultHasher::new();
            let optional: Option<OrderedFloat<f64>> = if self.is_null(i) {
                None
            } else {
                Some(self.value(i).into())
            };
            optional.hash(&mut hasher);
            builder.append_value(hasher.finish())?;
        }
        Ok(builder.finish())
    }
}

pub fn hash(array: &ArrayRef) -> ArrowResult<UInt64Array> {
    switch_array_unary_op!(array, "hash", hash_array)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::array::{null_primitive_array, ToArrayRef};
    use crate::types::ToArrowType;
    use arrow::datatypes::ArrowPrimitiveType;
    use std::sync::Arc;

    fn hash_it(x: impl Hash) -> u64 {
        let mut s = DefaultHasher::new();
        x.hash(&mut s);
        s.finish()
    }

    fn create_list_array<V, U, T>(builder: PrimitiveBuilder<V>, data: T) -> ArrowResult<ArrayRef>
    where
        T: AsRef<[Option<U>]>,
        U: AsRef<[V::Native]>,
        V: ArrowPrimitiveType,
    {
        let mut builder = ListBuilder::new(builder);
        for d in data.as_ref() {
            if let Some(v) = d {
                builder.values().append_slice(v.as_ref())?;
                builder.append(true)?
            } else {
                builder.append(false)?
            }
        }
        Ok(Arc::new(builder.finish()))
    }

    #[test]
    fn hash_u8() {
        let result = hash(&[42u8].to_array_ref()).unwrap();
        assert_eq!(result.len(), 1);
        assert_eq!(result.value(0), hash_it(Some(42u8)));
    }

    #[test]
    fn hash_i8() {
        let result = hash(&[42i8].to_array_ref()).unwrap();
        assert_eq!(result.len(), 1);
        assert_eq!(result.value(0), hash_it(Some(42i8)));
    }

    #[test]
    fn hash_f64() {
        let result = hash(&[42f64].to_array_ref()).unwrap();
        assert_eq!(result.len(), 1);
        assert_eq!(result.value(0), hash_it(Some(OrderedFloat(42f64))));
    }

    #[test]
    fn hash_null() {
        let array: ArrayRef = Arc::new(null_primitive_array::<f64>(1));
        let result = hash(&array).unwrap();
        assert_eq!(result.len(), 1);
        assert_eq!(result.value(0), hash_it(Option::<OrderedFloat<f64>>::None));
    }

    #[test]
    fn hash_unsupported() {
        let result = hash(&create_list_array(UInt8Builder::new(1), &[Some(&[42u8])]).unwrap());
        assert!(result.is_err());
    }
}
