use crate::compute::reorder::reorder;
use crate::compute::utils::SizedTable;
use crate::record_batch::RecordBatchFunctions;
use crate::ArrowResult;
use arrow::array::ArrayRef;
use arrow::record_batch::RecordBatch;
use std::cmp::Ordering;

pub fn argsort<T>(data: &T, comparator: impl Fn(&T, usize, usize) -> Ordering) -> Vec<usize>
where
    T: SizedTable,
{
    let mut indices: Vec<usize> = (0..data.rows()).collect();
    indices.sort_by(|&a, &b| comparator(data, a, b));
    indices
}

pub fn sort(
    data: &RecordBatch,
    comparator: impl Fn(&RecordBatch, usize, usize) -> Ordering,
) -> ArrowResult<RecordBatch> {
    let new_order = argsort(data, comparator);
    let columns: ArrowResult<Vec<ArrayRef>> = data
        .iter_columns()
        .map(|col| reorder(&new_order, col))
        .collect();
    Ok(RecordBatch::try_new(data.schema().clone(), columns?)?)
}
