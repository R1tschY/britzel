use arrow::array::{Array, ArrayData, ArrayDataRef, ArrayRef};
use arrow::record_batch::RecordBatch;

pub trait SizedTable {
    fn rows(&self) -> usize;
}

impl<T> SizedTable for [T] {
    fn rows(&self) -> usize {
        self.len()
    }
}

impl<T> SizedTable for Vec<T> {
    fn rows(&self) -> usize {
        self.len()
    }
}

impl SizedTable for ArrayRef {
    fn rows(&self) -> usize {
        self.len()
    }
}

impl SizedTable for ArrayData {
    fn rows(&self) -> usize {
        self.len()
    }
}

impl SizedTable for RecordBatch {
    fn rows(&self) -> usize {
        self.num_rows()
    }
}
