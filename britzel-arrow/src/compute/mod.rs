use arrow::array::ArrayRef;
macro_rules! switch_array_unary_op {
    ($switch:expr, $name:expr, $func:ident) => {{
        use arrow::array::*;
        use arrow::datatypes::DataType::*;
        match $switch.data_type() {
            UInt8 => crate::compute::downcast::<UInt8Array>($switch).$func(),
            UInt16 => crate::compute::downcast::<UInt16Array>($switch).$func(),
            UInt32 => crate::compute::downcast::<UInt32Array>($switch).$func(),
            UInt64 => crate::compute::downcast::<UInt64Array>($switch).$func(),
            Int8 => crate::compute::downcast::<Int8Array>($switch).$func(),
            Int16 => crate::compute::downcast::<Int16Array>($switch).$func(),
            Int32 => crate::compute::downcast::<Int32Array>($switch).$func(),
            Int64 => crate::compute::downcast::<Int64Array>($switch).$func(),
            Float32 => crate::compute::downcast::<Float32Array>($switch).$func(),
            Float64 => crate::compute::downcast::<Float64Array>($switch).$func(),
            Boolean => crate::compute::downcast::<BooleanArray>($switch).$func(),
            other => Err(crate::ArrowError::ComputeError(format!(
                "{} not supported for {:?}",
                $name, other
            ))),
        }
    }};
}

macro_rules! switch_array_binary_op {
    ($switch:expr, $name:expr, $func:ident, $arg:expr) => {{
        use arrow::array::*;
        use arrow::datatypes::DataType::*;
        match $switch.data_type() {
            UInt8 => crate::compute::downcast::<UInt8Array>($switch).$func($arg),
            UInt16 => crate::compute::downcast::<UInt16Array>($switch).$func($arg),
            UInt32 => crate::compute::downcast::<UInt32Array>($switch).$func($arg),
            UInt64 => crate::compute::downcast::<UInt64Array>($switch).$func($arg),
            Int8 => crate::compute::downcast::<Int8Array>($switch).$func($arg),
            Int16 => crate::compute::downcast::<Int16Array>($switch).$func($arg),
            Int32 => crate::compute::downcast::<Int32Array>($switch).$func($arg),
            Int64 => crate::compute::downcast::<Int64Array>($switch).$func($arg),
            Float32 => crate::compute::downcast::<Float32Array>($switch).$func($arg),
            Float64 => crate::compute::downcast::<Float64Array>($switch).$func($arg),
            Boolean => crate::compute::downcast::<BooleanArray>($switch).$func($arg),
            other => Err(crate::ArrowError::ComputeError(format!(
                "{} not supported for {:?}",
                $name, other
            ))),
        }
    }};
}

pub mod hash;
pub mod reorder;
pub mod sort;
pub mod utils;

pub(crate) fn downcast<T: 'static>(array: &ArrayRef) -> &T {
    array.as_any().downcast_ref::<T>().unwrap()
}
