use crate::{ArrowError, ArrowResult};
use arrow::array::{Array, ArrayRef, UInt64Array};
use std::sync::Arc;

trait Reorder {
    fn reorder_array(&self, new_order: &[usize]) -> ArrowResult<ArrayRef>;
}

macro_rules! impl_reorder_array {
    ($array_type:ty) => {
        impl Reorder for $array_type {
            fn reorder_array(&self, new_order: &[usize]) -> ArrowResult<ArrayRef> {
                let mut builder = <$array_type>::builder(new_order.len());
                for new_index in new_order {
                    if self.is_null(*new_index) {
                        builder.append_null()?;
                    } else {
                        builder.append_value(self.value(*new_index))?;
                    }
                }
                Ok(Arc::new(builder.finish()))
            }
        }
    };
}

impl_reorder_array!(arrow::array::UInt8Array);
impl_reorder_array!(arrow::array::UInt16Array);
impl_reorder_array!(arrow::array::UInt32Array);
impl_reorder_array!(arrow::array::UInt64Array);
impl_reorder_array!(arrow::array::Int8Array);
impl_reorder_array!(arrow::array::Int16Array);
impl_reorder_array!(arrow::array::Int32Array);
impl_reorder_array!(arrow::array::Int64Array);
impl_reorder_array!(arrow::array::Float32Array);
impl_reorder_array!(arrow::array::Float64Array);
impl_reorder_array!(arrow::array::BooleanArray);

pub fn reorder(new_order: &[usize], array: &ArrayRef) -> ArrowResult<ArrayRef> {
    if new_order.len() != array.len() {
        return Err(ArrowError::InvalidArgumentError(format!(
            "Array length mismatch on call of reorder: {} != {}",
            new_order.len(),
            array.len()
        )));
    }

    switch_array_binary_op!(array, "reorder", reorder_array, new_order)
}
