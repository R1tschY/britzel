use arrow::buffer::Buffer;
use arrow::array::{ArrayData, Array};
use arrow::record_batch::RecordBatch;
use crate::record_batch::RecordBatchFunctions;

pub trait DataSize {
    /// Size of data in bytes to stucture references to
    fn data_size(&self) -> usize;
}

impl DataSize for Buffer {
    fn data_size(&self) -> usize {
        self.len()
    }
}

impl DataSize for ArrayData {
    fn data_size(&self) -> usize {
        self.buffers().iter().map(|buffer| buffer.data_size()).sum::<usize>()
            + self.child_data().iter().map(|data| data.data_size()).sum::<usize>()
    }
}

impl DataSize for dyn Array {
    fn data_size(&self) -> usize {
        self.data_ref().data_size()
    }
}

impl DataSize for RecordBatch {
    fn data_size(&self) -> usize {
        self.iter_columns().map(|col| col.data_size()).sum()
    }
}