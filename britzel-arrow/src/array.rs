use crate::types::ToArrowType;
use arrow::array::{
    Array, ArrayData, ArrayRef, BinaryArray, BinaryBuilder, PrimitiveArray, PrimitiveBuilder,
    StringArray, StringBuilder,
};
use arrow::buffer::Buffer;
use arrow::datatypes::{
    ArrowPrimitiveType, BooleanType, DataType, Float32Type, Float64Type, Int16Type, Int32Type,
    Int64Type, Int8Type, ToByteSlice, UInt16Type, UInt32Type, UInt64Type, UInt8Type,
};
use std::sync::Arc;

pub trait ToArray<T: Array> {
    fn to_array(&self) -> T;
}

pub trait ToArrayRef {
    fn to_array_ref(&self) -> ArrayRef;
}

macro_rules! impl_to_primitive_array {
    ($ty:ident, $native_ty:ident, $ty_id:expr) => {
        //        impl ToArray<PrimitiveArray<$ty>> for &[$native_ty] {
        //            fn to_array(&self) -> PrimitiveArray<$ty> {
        //                let array_data = ArrayData::builder($ty_id)
        //                    .len(self.len())
        //                    .add_buffer(Buffer::from(self.to_byte_slice()))
        //                    .build();
        //                PrimitiveArray::from(array_data)
        //            }
        //        }

        impl ToArray<PrimitiveArray<$ty>> for [$native_ty] {
            fn to_array(&self) -> PrimitiveArray<$ty> {
                let array_data = ArrayData::builder($ty_id)
                    .len(self.len())
                    .add_buffer(Buffer::from(self.to_byte_slice()))
                    .build();
                PrimitiveArray::from(array_data)
            }
        }

        impl ToArrayRef for [$native_ty] {
            fn to_array_ref(&self) -> Arc<dyn Array> {
                Arc::new(self.to_array())
            }
        }
    };
}

impl_to_primitive_array!(BooleanType, bool, DataType::Boolean);
impl_to_primitive_array!(Int8Type, i8, DataType::Int8);
impl_to_primitive_array!(Int16Type, i16, DataType::Int16);
impl_to_primitive_array!(Int32Type, i32, DataType::Int32);
impl_to_primitive_array!(Int64Type, i64, DataType::Int64);

impl_to_primitive_array!(UInt8Type, u8, DataType::UInt8);
impl_to_primitive_array!(UInt16Type, u16, DataType::UInt16);
impl_to_primitive_array!(UInt32Type, u32, DataType::UInt32);
impl_to_primitive_array!(UInt64Type, u64, DataType::UInt64);

impl_to_primitive_array!(Float32Type, f32, DataType::Float32);
impl_to_primitive_array!(Float64Type, f64, DataType::Float64);

pub fn full_primitive_array<T: ArrowPrimitiveType>(t: &T::Native, len: usize) -> PrimitiveArray<T> {
    let mut builder = PrimitiveBuilder::<T>::new(len);
    for _ in 0..len {
        builder.append_value(*t);
    }
    builder.finish()
}

pub fn null_primitive_array<T: ToArrowType>(len: usize) -> PrimitiveArray<T::ArrowType>
where
    <T as ToArrowType>::ArrowType: arrow::datatypes::ArrowPrimitiveType,
{
    let mut builder = PrimitiveBuilder::<T::ArrowType>::new(len);
    for _ in 0..len {
        builder.append_null();
    }
    builder.finish()
}

pub fn full_string_array(t: &str, len: usize) -> StringArray {
    let mut builder = StringBuilder::new(len);
    for _ in 0..len {
        builder.append_value(t);
    }
    builder.finish()
}

pub fn null_string_array(len: usize) -> BinaryArray {
    let mut builder = BinaryBuilder::new(len);
    for _ in 0..len {
        builder.append_null();
    }
    builder.finish()
}
