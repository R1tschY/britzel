/*

pub trait CodeGenC {
    fn codegen_c(&self) -> String;
}

impl CodeGenC for Expression {
    fn codegen_c(&self) -> String {
        use expressions::Expression::*;

        match self {
            &Add(ref left, ref right) =>
                format!("({}) + ({})", left.codegen_c(), right.codegen_c()),
            &Sub(ref left, ref right) =>
                format!("({}) - ({})", left.codegen_c(), right.codegen_c()),
            &Mul(ref left, ref right) =>
                format!("({}) * ({})", left.codegen_c(), right.codegen_c()),
            &Div(ref left, ref right) =>
                format!("({}) / ({})", left.codegen_c(), right.codegen_c()),
            &Var(ref name) => name.clone(),

            Literal(lit) => lit.codegen_c(),
        }
    }
}

impl CodeGenC for LiteralType {
    fn codegen_c(&self) -> String {
        use queries::schema::DataType::*;

        match self {
             U8(lit) => format!("((uint8_t){})", lit),
             I8(lit) => format!("((int8_t){})", lit),
            U16(lit) => format!("((uint16_t){})", lit),
            I16(lit) => format!("((int16_t){})", lit),
            U32(lit) => format!("((uint32_t){})", lit),
            I32(lit) => format!("((int32_t){})", lit),
            U64(lit) => format!("((uint64_t){})", lit),
            I64(lit) => format!("((int64_t){})", lit),
            F32(lit) => format!("{}f", lit),
            F64(lit) => format!("{}", lit),
            Str(lit) => format!("\"{}\"", lit), // TODO: escape
        }
    }
}

impl CodeGenC for Operation {
    fn codegen_c(&self) -> String {
        String::new()
    }
}*/