use crate::error::CoreError::InternalError;
use crate::error::CoreResult;
use arrow::record_batch::RecordBatch;
use std::collections::HashMap;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Mutex;

pub type BroadcastId = u32;

/// user object for broadcasts
pub struct Broadcast(BroadcastId);

pub(crate) struct BroadcastRegistry {
    next_broadcast_id: AtomicU32,
}

impl BroadcastRegistry {
    fn next_broadcast_id(&self) -> u32 {
        self.next_broadcast_id.fetch_add(1, Ordering::Relaxed)
    }

    pub fn create_broadcast(&self, value: RecordBatch) -> CoreResult<BroadcastId> {
        let id = self.next_broadcast_id();
        // {
        //     let mut broadcasts = self
        //         .broadcasts
        //         .lock()
        //         .map_err(|_| InternalError("posined broadcast registry mutex".to_string()))?;
        //     broadcasts.insert(id, value);
        // }

        // TODO: push to executors

        Ok(id)
    }

    pub fn read_broadcast(&self, id: BroadcastId) -> CoreResult<Option<RecordBatch>> {
        let mut broadcasts = self
            .broadcasts
            .lock()
            .map_err(|_| InternalError("posined broadcast registry mutex".to_string()))?;
        Ok(broadcasts.get(&id).cloned())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sended_broadcast_can_be_read() {}
}
