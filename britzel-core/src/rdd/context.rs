use crate::broadcast::BroadcastId;
use crate::rdd::RddRef;
use arrow::record_batch::RecordBatch;
use std::cell::Cell;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::Arc;

pub struct RddContext {
    next_rdd_id: AtomicU32,
}

impl RddContext {
    pub fn new() -> Arc<Self> {
        Arc::new(Self {
            next_rdd_id: AtomicU32::new(0),
        })
    }

    pub(crate) fn next_rdd_id(&self) -> u32 {
        self.next_rdd_id.fetch_add(1, Ordering::Relaxed)
    }

    pub fn parallelize(self: Arc<RddContext>, data: RecordBatch) -> RddRef {
        RddRef::parallelize(self, data)
    }

    pub fn parallelize_record_batches<T: IntoIterator<Item = RecordBatch>>(
        self: Arc<RddContext>,
        data: T,
    ) -> RddRef {
        RddRef::parallelize_record_batches(self, data)
    }

    /// broadcast `value` to every cluster node
    ///
    /// Data will be send only once to each node and is available for distributed functions.
    pub fn broadcast(value: RecordBatch) -> BroadcastId {}
}
