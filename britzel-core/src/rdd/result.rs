use crate::britzel_arrow::array::ToArray;
use crate::rdd::RddResult;
use arrow::record_batch::RecordBatch;
use britzel_arrow::record_batch::{record_batch_from_array, RecordBatchFunctions};

pub(super) enum WorkerPartitionAgg {
    Count,
    Collect,
    Take(u64),
}

pub(super) trait RddAggregation {
    fn worker_agg(&self) -> WorkerPartitionAgg;

    fn update(&mut self, partition: RecordBatch);
    fn result(self) -> RddResult;
}

pub(super) struct CountAgg(u64);

impl RddAggregation for CountAgg {
    fn worker_agg(&self) -> WorkerPartitionAgg {
        WorkerPartitionAgg::Count
    }

    fn update(&mut self, partition: RecordBatch) {
        self.0 += partition.typed_value::<u64>(0, 0);
    }

    fn result(self) -> RddResult {
        Ok(vec![record_batch_from_array([self.0].to_array())])
    }
}

pub(super) struct Collect(Vec<RecordBatch>);

impl RddAggregation for Collect {
    fn worker_agg(&self) -> WorkerPartitionAgg {
        WorkerPartitionAgg::Collect
    }

    fn update(&mut self, partition: RecordBatch) {
        // TODO: check schema
        self.0.push(partition);
    }

    fn result(self) -> RddResult {
        Ok(self.0)
    }
}

pub(super) struct Take {
    remaining: u64,
    result: Vec<RecordBatch>,
}

impl Take {
    pub fn new(limit: u64) -> Self {
        Take {
            remaining: limit,
            result: vec![],
        }
    }
}

impl RddAggregation for Take {
    fn worker_agg(&self) -> WorkerPartitionAgg {
        WorkerPartitionAgg::Take(self.remaining)
    }

    fn update(&mut self, partition: RecordBatch) {
        // TODO: check schema
        if self.remaining > partition.num_rows() as u64 {
            self.remaining -= partition.num_rows() as u64;
            self.result.push(partition);
        } else if self.remaining != 0 {
            self.remaining = 0;
            self.result
                .push(partition.take(partition.num_rows() - self.remaining as usize));
        }
    }

    fn result(self) -> RddResult {
        Ok(self.result)
    }
}
