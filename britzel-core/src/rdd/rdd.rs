use crate::rdd::context::RddContext;
use crate::rdd::partition::PartitionRef;
use crate::rdd::{ParallelizePartition, Partition, PartitionId};
use crate::scheduler::task_context::TaskContext;
use arrow::record_batch::RecordBatch;
use std::io;
use std::ops::Deref;
use std::sync::Arc;

pub enum PartitionDependency {
    ShuffleDependency(),
    NarrowDependency(Box<dyn Fn(PartitionId) -> Vec<PartitionId> + Send + Sync + 'static>),
}

pub type RddId = u32;

#[derive(Clone)]
pub struct RddRef(Arc<Rdd>);

pub struct Dependency {
    rdd: RddRef,
    descr: PartitionDependency,
}

impl Dependency {
    pub fn new_one_to_one(rdd: RddRef) -> Dependency {
        Dependency {
            rdd,
            descr: PartitionDependency::NarrowDependency(Box::new(|pid| vec![pid])),
        }
    }
}

pub struct Rdd {
    id: RddId,
    context: Arc<RddContext>,
    dependencies: Vec<Dependency>,
    partitions: Vec<PartitionRef>,
}

impl Rdd {
    pub fn new(
        context: Arc<RddContext>,
        dependencies: Vec<Dependency>,
        partitions: Vec<PartitionRef>,
    ) -> Rdd {
        Self {
            id: context.next_rdd_id(),
            context,
            dependencies,
            partitions,
        }
    }

    pub fn dependencies(&self) -> &[Dependency] {
        &self.dependencies
    }

    pub fn partitions(&self) -> &[PartitionRef] {
        &self.partitions
    }

    pub fn context(&self) -> Arc<RddContext> {
        Arc::clone(&self.context)
    }
}

impl RddRef {
    pub fn new(
        context: Arc<RddContext>,
        dependencies: Vec<Dependency>,
        partitions: Vec<PartitionRef>,
    ) -> RddRef {
        RddRef(Arc::new(Rdd::new(context, dependencies, partitions)))
    }
}

impl Deref for RddRef {
    type Target = Rdd;

    fn deref(&self) -> &Rdd {
        self.0.deref()
    }
}

impl From<Arc<Rdd>> for RddRef {
    fn from(r: Arc<Rdd>) -> Self {
        RddRef(r.clone())
    }
}

impl From<Rdd> for RddRef {
    fn from(r: Rdd) -> Self {
        RddRef(Arc::new(r))
    }
}
