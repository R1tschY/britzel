use std::iter::IntoIterator;
use std::sync::Arc;
use std::io;
use arrow::record_batch::RecordBatch;
use britzel_arrow::array::ToArray;
use crate::rdd::{RddRef, Rdd, ParallelizePartition, PartitionRef, Dependency, PartitionId, Partition};
use crate::rdd::context::RddContext;


impl RddRef {
    pub fn parallelize(context: Arc<RddContext>, data: RecordBatch) -> RddRef {
        Self::create_root_rdd(context, vec![ParallelizePartition::new(Arc::new(data))])
    }

    pub fn parallelize_record_batches<T: IntoIterator<Item=RecordBatch>>(context: Arc<RddContext>, data: T) -> RddRef {
        Self::create_root_rdd(
            context,
            data.into_iter().map(|p| ParallelizePartition::new(Arc::new(p))).collect())
    }

    pub fn map_partitions<Out: Partition + 'static, T: Fn(PartitionRef) -> Out>(&self, f: T) -> RddRef {
        self.create_map_partitions_rdd(
            self,
            self.partitions().iter().map(|part| -> PartitionRef {
                Arc::new(f(Arc::clone(part)))
            }).collect(),
        )
    }

    pub fn map_partitions_with_index<Out: Partition + 'static, T: Fn(PartitionId, PartitionRef) -> Out>(&self, f: T) -> RddRef {
        self.create_map_partitions_rdd(
            self,
            self.partitions().iter().enumerate().map(|(idx, part)| -> PartitionRef {
                Arc::new(f(idx as PartitionId, Arc::clone(part)))
            }).collect(),
        )
    }

    // private helper

    fn create_root_rdd(context: Arc<RddContext>, data: Vec<PartitionRef>) -> RddRef {
        Self::new(context, vec![], data)
    }

    fn create_map_partitions_rdd(&self, source: &RddRef, data: Vec<PartitionRef>) -> RddRef {
        Self::new(self.context(), vec![Dependency::new_one_to_one(source.clone())], data)
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use arrow::datatypes::{Schema, DataType, Field};
    use arrow::array::*;
    use crate::rdd::{Partition, RddResult, PartitionResult};
    use crate::scheduler::task_context::TaskContext;
    use tokio::io::Error;
    use britzel_arrow::record_batch::{record_batch_from_slice, record_batch_from_array};

    #[test]
    fn test_parallelize_signature() {
        let ctx = RddContext::new();
        ctx.parallelize(record_batch_from_array([1i8, 2, 3, 4, 5].to_array()));
    }

    struct NoOpPartition(PartitionRef);

    impl Partition for NoOpPartition {
        fn compute(&self, ctx: &TaskContext) -> PartitionResult {
            self.0.compute(ctx)
        }
    }

    #[test]
    fn test_map_partitions_signature() {
        let ctx = RddContext::new();
        ctx.parallelize(record_batch_from_array([1i8, 2, 3, 4, 5].to_array()))
            .map_partitions(|p| NoOpPartition(p));
    }
}