use arrow::record_batch::RecordBatch;
use std::io;

mod functions;
mod partition;
mod rdd;
mod result;
mod context;

pub type PartitionResult = io::Result<RecordBatch>;
pub type RddResult = io::Result<Vec<RecordBatch>>;

pub type PartitionId = u32;
pub type PartitionCount = u32;

pub use crate::rdd::partition::{ParallelizePartition, Partition, PartitionRef};
pub use crate::rdd::rdd::{Dependency, PartitionDependency, Rdd, RddId, RddRef};
