use std::sync::Arc;
use crate::scheduler::task_context::TaskContext;
use std::io;
use arrow::record_batch::RecordBatch;
use crate::rdd::{RddResult, PartitionResult};

pub type PartitionRef = Arc<dyn Partition>;

//#[typetag::serde(tag = "type")]
pub trait Partition : Send + Sync {
    fn compute(&self, ctx: &TaskContext) -> PartitionResult;
}

//#[derive(Serialize, Deserialize)]
pub struct ParallelizePartition(Arc<RecordBatch>);

//#[typetag::serde]
impl Partition for ParallelizePartition {
    fn compute(&self, ctx: &TaskContext) -> PartitionResult {
        Ok(RecordBatch::clone(&self.0))
    }
}

impl ParallelizePartition {
    pub fn new(data: Arc<RecordBatch>) -> PartitionRef {
        Arc::new(ParallelizePartition(data)) as PartitionRef
    }
}