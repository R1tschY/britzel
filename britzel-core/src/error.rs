quick_error! {
    #[derive(Debug)]
    pub enum CoreError {
        Arrow(err: arrow::error::ArrowError) {
            from()
            description(err.description())
            display("Arrow error: {:?}", err)
        }
        Unimplemented(message: String) {
            display("Not yet supported feature: {}", message)
            description(message)
        }
        InternalError(message: String) {
            display("Internal error: {}", message)
            description(message)
        }
    }
}

pub type CoreResult<T> = Result<T, CoreError>;
