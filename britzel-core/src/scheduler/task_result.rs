use arrow::record_batch::RecordBatch;
use std::io;


#[derive(Debug)]
pub enum TaskError {
    ExecutorLost(String),
    TaskCanceled(String),
    TaskPanicked(String),
    TaskError(io::Error)
}

pub type TaskResult = Result<RecordBatch, TaskError>;