use std::cell::{Cell};
use std::collections::HashMap;
use std::io;
use std::thread;
use std::sync::{Condvar, Arc, Mutex};

use actix::prelude::*;
use actix::Addr;
use arrow::record_batch::RecordBatch;
use tokio::prelude::*;
use tokio::sync::mpsc::error::UnboundedSendError;
use tokio::sync::oneshot;

use SchedulerEvent::*;

use crate::rdd::RddRef;
use crate::scheduler::result_agg::ResultAgg;
use crate::scheduler::task_result::TaskResult;

use super::task::{Task, TaskSet};

enum SchedulerEvent {
    SubmitJob {
        result: oneshot::Sender<io::Result<RecordBatch>>,
        rdd: RddRef,
        partitions: Vec<u32>,
        agg: Box<dyn ResultAgg>
    },

    // CancelJob
    //ExecutorAdded,ExecutorLost
    // CompletionEvent

    StopScheduler
}

impl Message for SchedulerEvent {
    type Result = io::Result<()>;
}

struct SchedulerJob {
    job_id: u32,
    result: oneshot::Sender<io::Result<RecordBatch>>
}


/// Interface to the scheduler
pub struct SchedulerRef {
    tx: Addr<SchedulerActor>
}

fn shutdown_err() -> io::Error {
    io::Error::new(io::ErrorKind::NotFound, "scheduler was shutdown")
}


impl SchedulerRef {
    fn new(tx: Addr<SchedulerActor>) -> Self {
        Self { tx }
    }

    pub(crate) fn submit_job(
        &mut self, rdd: RddRef, partitions: &[u32], agg: Box<dyn ResultAgg>
    ) -> Box<dyn Future<Item=RecordBatch, Error=io::Error>> {
        let (tx, rx) = oneshot::channel();

        if let Err(_) = self.tx.try_send(SubmitJob {
            result: tx,
            rdd,
            partitions: partitions.into(),
            agg
        }) {
           return Box::new(future::err(shutdown_err()));
        }

        Box::new(rx.map_err(|_| shutdown_err()).and_then(|res| res))
    }

    pub(crate) fn stop(self) {
        drop(self)
    }
}

pub struct SchedulerActor {
    last_job_id: Cell<u32>,
    jobs: HashMap<u32, SchedulerJob>
}

impl SchedulerActor {
    fn handle_submit_job(
        &mut self,
        result: oneshot::Sender<io::Result<RecordBatch>>,
        rdd: RddRef,
        partitions: Vec<u32>,
        agg: Box<dyn ResultAgg>
    ) {
        // TODO: generate stage
        let job_id = self.gen_job_id();
        let rdd_partitions = rdd.partitions();
        let tasks: Vec<Task> = partitions.iter().map(|part| {
            Task::new_result_task(
                0,
                rdd_partitions[*part as usize].clone(),
                0,
                0,
                *part,
                job_id,
                None,
                None
            )
        }).collect();

        self.jobs.insert(job_id, SchedulerJob { job_id, result });

        self.submit_tasks(agg, TaskSet {
            tasks,
            stage_id: 0,
            stage_attempt_id: 0,
            job_id
        });
    }

    fn agg_task_results(&mut self, mut agg: Box<dyn ResultAgg>, tasks: &TaskSet) -> io::Result<RecordBatch> {
        for task in tasks.tasks.iter() {
            match task.run() {
                Ok(value) => agg.as_mut().update(&value),
                Err(err) => return Err(err)
            }
        }

        Ok(agg.result())
    }

    fn submit_tasks(&mut self, agg: Box<dyn ResultAgg>, tasks: TaskSet) {
        // Executor and TaskRunner missing

        let job_id = tasks.job_id;
        let result = self.agg_task_results(agg, &tasks);
        if let Some(job) = self.jobs.remove(&job_id) {
            if let Err(_) = job.result.send(result) {
                warn!("Result to job {} cannot send back: other end is closed", job_id);
            }
        } else {
            // job does not exist anymore
            warn!("Result to job {} cannot send back: job result already send", job_id);
        }
    }

    fn gen_job_id(&mut self) -> u32 {
        let result = self.last_job_id.get();
        self.last_job_id.set(result + 1);
        result
    }

    pub fn run() -> SchedulerRef {
        let pair = Arc::new((Mutex::<Option<Addr<SchedulerActor>>>::new(None), Condvar::new()));
        let pair2 = Arc::clone(&pair);

        thread::spawn(move || {
            actix::System::run(move || {
                let (lock, cvar) = &*pair2;
                let mut started = lock.lock().unwrap();

                *started = Some(SchedulerActor {
                    last_job_id: Cell::new(0),
                    jobs: HashMap::new()
                }.start());
                cvar.notify_one();
            }).unwrap();
        });

        let (lock, cvar) = &*pair;
        let mut started = lock.lock().unwrap();
        while (*started).is_none() {
            started = cvar.wait(started).unwrap();
        }

        SchedulerRef::new(started.as_ref().unwrap().clone())
    }
}

impl Actor for SchedulerActor {
    type Context = Context<Self>;
}

impl Handler<SchedulerEvent> for SchedulerActor {
    type Result = io::Result<()>;

    fn handle(&mut self, msg: SchedulerEvent, ctx: &mut Self::Context) -> Self::Result {
        match msg {
            SubmitJob { result, rdd, partitions, agg } =>
                self.handle_submit_job(result, rdd, partitions, agg),
            StopScheduler => System::current().stop()
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use arrow::array::{UInt32Array, UInt64Array};

    use britzel_arrow::record_batch::record_batch_from_slice;
    use crate::rdd::Rdd;
    use crate::scheduler::result_agg::CountRowsResultAgg;

    use super::*;

/*    #[tokio::test]
    async fn test_submit_job() {
        let rdd = Rdd::parallelize_raw(vec![
            record_batch_from_1(&[42u32] as &[u32])
        ]);

        let mut scheduler = Scheduler::run();
        let result = scheduler.submit_job(
            rdd, &[0], CountRowsResultAgg::new()).await.unwrap();

        assert_eq!(result.column(0).as_any().downcast_ref::<UInt64Array>().unwrap().value(0), 1);

        scheduler.stop();
    }*/
}