use crate::rdd::PartitionRef;
use crate::scheduler::task_context::TaskContext;
use arrow::record_batch::RecordBatch;
use std::io;

/// Runs task von executor side
enum TaskImpl {
    ResultTask { output_id: u32 },
    ShuffleTask,
}

enum TaskData {
    ResultTask(PartitionRef),
    ShuffleTask,
}

/// Information about execution of task
pub(crate) struct Task {
    task: TaskImpl,
    task_binary: TaskData,
    partition_id: u32,

    // TODO: to Arc<StageId>
    stage_id: u32,
    stage_attempt_id: u32,
    job_id: u32,
    app_id: Option<String>,
    app_attempt_id: Option<String>,
}

impl Task {
    pub fn new_result_task(
        output_id: u32,
        partition: PartitionRef,
        stage_id: u32,
        stage_attempt_id: u32,
        partition_id: u32,
        job_id: u32,
        app_id: Option<String>,
        app_attempt_id: Option<String>,
    ) -> Self {
        Self {
            task: TaskImpl::ResultTask { output_id },
            task_binary: TaskData::ResultTask(partition),
            stage_id,
            stage_attempt_id,
            partition_id,
            job_id,
            app_id,
            app_attempt_id,
        }
    }

    pub fn stage_id(&self) -> u32 {
        self.stage_id
    }
    pub fn stage_attempt_id(&self) -> u32 {
        self.stage_attempt_id
    }
    pub fn partition_id(&self) -> u32 {
        self.partition_id
    }
    pub fn job_id(&self) -> u32 {
        self.job_id
    }
    pub fn app_id(&self) -> Option<String> {
        self.app_id.clone()
    }
    pub fn app_attempt_id(&self) -> Option<String> {
        self.app_attempt_id.clone()
    }

    pub fn run(&self) -> io::Result<RecordBatch> {
        let ctx = TaskContext::new(self);
        self.run_task(&ctx)
    }

    fn run_task(&self, ctx: &TaskContext) -> io::Result<RecordBatch> {
        // TODO: deserialize task and take time
        match &self.task_binary {
            TaskData::ResultTask(part) => part.compute(ctx),
            TaskData::ShuffleTask => unimplemented!(),
        }
    }
}

pub(crate) struct TaskSet {
    pub tasks: Vec<Task>,
    pub job_id: u32,
    pub stage_id: u32,
    pub stage_attempt_id: u32,
}
