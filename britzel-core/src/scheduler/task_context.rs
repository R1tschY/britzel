use crate::scheduler::task::Task;

pub struct TaskContext<'a> {
    task: &'a Task
}

impl<'a> TaskContext<'a> {
    pub(super) fn new(task: &'a Task) -> Self {
        TaskContext { task }
    }

    pub fn stage_id(&self) -> u32 { self.task.stage_id() }
    pub fn stage_attempt_id(&self) -> u32 { self.task.stage_attempt_id() }
    pub fn partition_id(&self) -> u32 { self.task.partition_id() }
    pub fn job_id(&self) -> u32 { self.task.job_id() }
    pub fn app_id(&self) -> Option<String> { self.task.app_id() }
    pub fn app_attempt_id(&self) -> Option<String> { self.task.app_attempt_id() }
}


