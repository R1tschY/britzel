use arrow::record_batch::RecordBatch;
use arrow::datatypes::{Schema, DataType, Field};
use britzel_arrow::array::ToArray;
use std::sync::Arc;

pub(crate) trait ResultAgg : Send + Sync {
    fn update(&mut self, input: &RecordBatch);
    fn result(&self) -> RecordBatch;
}

pub struct CountRowsResultAgg(u64);

impl CountRowsResultAgg {
    pub(crate) fn new() -> Box<dyn ResultAgg> {
        Box::new(CountRowsResultAgg(0))
    }
}

impl ResultAgg for CountRowsResultAgg {
    fn update(&mut self, input: &RecordBatch) {
        self.0 += input.num_rows() as u64;
    }

    fn result(&self) -> RecordBatch {
        let schema = Schema::new(vec![
            Field::new("value", DataType::UInt64, false),
        ]);
        let array = [self.0].to_array();
        RecordBatch::try_new(Arc::new(schema), vec![Arc::new(array)]).unwrap()
    }
}