use actix::Message;
use std::io;
use std::error::Error;
use arrow::record_batch::RecordBatch;
use crate::scheduler::task_result::TaskResult;

enum TaskState {
    Launching,
    Running,
    Succeeded,
    Failed,
    Canceled,
    Lost
}

impl TaskState {
    pub fn is_failed(&self) -> bool {
        match self {
            TaskState::Failed | TaskState::Lost => true,
            _ => false,
        }
    }

    pub fn is_finished(&self) -> bool {
        match self {
            TaskState::Launching | TaskState::Running => false,
            _ => true,
        }
    }
}




pub enum TaskStatusEvent {
    TaskRunning,
    TaskFinished(TaskResult),
}

impl Message for TaskStatusEvent {
    type Result = ();
}