use crate::compile::compiler::CompileProfile;
use std::process::Command;
use std::process::Stdio;
use std::io;
use std::io::Write;
use std::io::Read;
use crate::compile::compiler::Language;
use crate::compile::compiler::CompilerSuite;
use crate::compile::compiler::CompileCommand;

pub struct Gcc { }

impl Gcc {
    fn command(language: Language) -> CompileCommand {
        CompileCommand::new(Box::new(Gcc { }), language)
    }
}

fn profile_flags(profile: CompileProfile) -> Vec<&'static str> {
    use crate::compile::compiler::CompileProfile::*;

    match profile {
        Debug => vec!["-Og", "-g"],
        Profile => vec!["-O3", "-fno-omit-framepointer"],
        Optimized => vec!["-O3"],
        OptimizedNative => vec!["-O3", "-march=native"],
    }
}

fn warning_flags() -> Vec<&'static str> {
    vec!["-Wall"]
}

fn language_flags(lang: Language) -> Vec<&'static str> {
    use self::Language::*;

    match lang {
        C99 => vec!["-x", "c", "-std=c99"],
        Cxx03 => vec!["-x", "c++", "-std=c++03"],
        Cxx11 => vec!["-x", "c++", "-std=c++0x"],
        Cxx14 => vec!["-x", "c++", "-std=c++1y"],
        Cxx17 => vec!["-x", "c++", "-std=c++1z"],
    }
}

impl CompilerSuite for Gcc {

    fn compile_string(&self, cmd: &CompileCommand, code: &str, dest: &str) -> io::Result<()> {
        let mut process = Command::new(cmd.get_path().unwrap_or(String::from("gcc")))
            .args(profile_flags(cmd.get_profile()))
            .args(warning_flags())
            .args(language_flags(cmd.get_language()))
            .args(["-shared", "-o", &dest, "-"].iter())
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;

        info!("Executing {:?}", process);

        write!(process.stdin.as_mut().unwrap(), "{}", code)?;
        let result_code = process.wait()?;

        let mut stderr = String::new();
        process.stderr.unwrap().read_to_string(&mut stderr)?;
        error!("gcc stderr: {}", stderr);  // TODO: for every line

        let mut stdout = String::new();
        process.stdout.unwrap().read_to_string(&mut stdout)?;
        warn!("gcc stdout: {}", stdout);  // TODO: for every line

        if result_code.success() {
            Ok(())
        } else {
            Err(io::Error::new(io::ErrorKind::Other, "gcc error"))
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate tempfile;
    extern crate libloading as lib;

    use super::*;
    use self::tempfile::Builder;

    #[test]
    fn compile_string() {
        let temp_file = Builder::new()
            .prefix("hlangtest_")
            .suffix(".mod")
            .tempfile()
            .unwrap();
        let temp_file_name = temp_file.path().to_str().unwrap();

        let code = r##"
            #include <cstdint>

            extern "C" std::int32_t test_function() {
                return 42;
            }
        "##;

        Gcc::command(Language::Cxx14)
            .compile_string(code, temp_file_name)
            .unwrap();

        let lib = lib::Library::new(temp_file_name).unwrap();
        unsafe {
            let func: lib::Symbol<unsafe extern fn() -> i32> = lib.get(b"test_function\0").unwrap();
            assert_eq!(func(), 42);
        }
    }
}