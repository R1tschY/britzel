use std::io;

#[derive(Copy, Clone)]
pub enum CompileProfile {
    /// Compile for debugging
    Debug,

    // Compile for profiling
    Profile,

    // Compile optimized (portable)
    Optimized,

    // Compile optimized for host arch
    OptimizedNative,
}

#[derive(Copy, Clone)]
pub enum Language {
    /// C99
    C99,

    /// C++03
    Cxx03,

    /// C++11
    Cxx11,

    /// C++14
    Cxx14,

    /// C++17
    Cxx17,
}

pub trait CompilerSuite {

    /// compiles `code` to dynamic library at `dest`
    fn compile_string(&self, cmd: &CompileCommand, code: &str, dest: &str) -> io::Result<()>;
}

pub struct CompileCommand {
    compiler_suite: Box<dyn CompilerSuite>,
    path: Option<String>,
    language: Language,
    extra_flags: Vec<String>,
    profile: CompileProfile
}


/// compiler abstraction
impl CompileCommand {
    pub fn new(compiler_suite: Box<dyn CompilerSuite>, language: Language) -> CompileCommand {
        CompileCommand {
            compiler_suite,
            path: None,
            language,
            extra_flags: vec![],
            profile: CompileProfile::OptimizedNative  // TODO: Debug on debug build
        }
    }

    /// add compile flag
    pub fn flag(&mut self, flag: &str) {
        self.extra_flags.push(String::from(flag));
    }

    /// set compile profile
    pub fn profile(&mut self, profile: CompileProfile) {
        self.profile = profile;
    }

    /// explicitly set path to compiler
    pub fn path(&mut self, path: &str) {
        self.path = Some(String::from(path));
    }

    pub fn get_path(&self) -> Option<String> { self.path.clone() }
    pub fn get_language(&self) -> Language { self.language.clone() }
    pub fn get_extra_flags(&self) -> Vec<String> { self.extra_flags.clone() }
    pub fn get_profile(&self) -> CompileProfile { self.profile.clone() }

    /// compiles `code` to dynamic library at `dest`
    pub fn compile_string(&self, code: &str, dest: &str) -> io::Result<()> {
        let suite = &self.compiler_suite;
        suite.compile_string(self, code, dest)
    }
}