#![feature(specialization)]
#![feature(trivial_bounds)]

#[macro_use]
extern crate log;
//#[macro_use] extern crate smallvec;
#[macro_use]
extern crate lazy_static;
//extern crate uuid;
extern crate arrow;
#[macro_use]
extern crate serde;
#[macro_use]
extern crate typetag;
extern crate actix;
extern crate futures;
extern crate tokio;
#[macro_use]
extern crate quick_error;
extern crate britzel_arrow;
extern crate britzel_query;

pub mod broadcast;
pub mod error;
pub mod executor;
pub mod rdd;
pub mod scheduler;
pub mod shuffle;

pub mod node;

//pub mod compile;
//pub mod codegen_c;
//mod session;

//pub use crate::session::Session;
