use serde::export::Formatter;
use snafu::{ensure, Backtrace, ErrorCompat, NoneError, ResultExt, Snafu};
use std::fmt;
use std::str::FromStr;
use std::sync::Arc;

#[derive(Debug, Snafu)]
pub enum NodeAddressError {
    #[snafu(display("Invalid port: {}", input))]
    InvalidPort { input: String },
}

#[derive(Eq, PartialEq, Hash, Clone)]
pub struct ServiceAddress {
    hostname: String,
    port: u16,
    // rack: Option<String>,
    // node: Option<String>,
    // data_center: Option<String>,
}

impl ServiceAddress {
    pub fn from_str(address: &str) -> Result<Self, NodeAddressError> {
        let parts: Vec<&str> = address.splitn(2, ':').collect();
        let port = u16::from_str(parts[1])
            .map_err(|_| NoneError)
            .context(InvalidPort {
                input: parts[1].to_string(),
            })?;

        Ok(Self {
            hostname: parts[0].to_string(),
            port,
        })
    }

    pub fn distance_to(&self, other: &ServiceAddress) -> RelativeLocation {
        if self.hostname == other.hostname {
            if self.port == other.port {
                RelativeLocation::Process
            } else {
                RelativeLocation::LogicalNode
            }
        } else {
            RelativeLocation::Web
        }
    }
}

impl fmt::Debug for ServiceAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_fmt(format_args!("{}:{}", self.hostname, self.port))
    }
}

#[derive(Eq, PartialEq, PartialOrd)]
pub enum RelativeLocation {
    /// same process
    Process,

    /// same logical node (docker container, VM, ...)
    /// Aka localhost
    LogicalNode,

    /// same physical node
    PhysicalNode,

    /// same rack
    Rack,

    /// same logical network
    Network,

    /// same data center
    DataCenter,

    /// farest distance possible
    Web,
}

impl fmt::Display for RelativeLocation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use RelativeLocation::*;

        f.write_str(match self {
            Process => "PROCESS_LOCAL",
            LogicalNode => "LOGICAL_NODE_LOCAL",
            PhysicalNode => "NODE_LOCAL",
            Rack => "RACK_LOCAL",
            Network => "NETWORK_LOCAL",
            DataCenter => "DATA_CENTER_LOCAL",
            Web => "WEB",
        })
    }
}
