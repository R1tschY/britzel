use crate::node::ServiceAddress;
use crate::scheduler::task_manager::TaskStatusEvent;
use actix::{Actor, Context, Handler};
use std::sync::Arc;
use threadpool::ThreadPool;

mod backend;
mod dispatcher;
mod worker;

const DRIVER_EXECUTOR_ID_STR: &str = "driver";

#[derive(Debug, Hash, Eq, PartialEq)]
pub struct ExecutorId(Arc<String>);

impl AsRef<str> for ExecutorId {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl ExecutorId {
    pub fn driver() -> ExecutorId {
        // TODO: lazy_static with DRIVER_EXECUTOR_ID_STR
        ExecutorId(Arc::new(DRIVER_EXECUTOR_ID_STR.to_string()))
    }

    pub fn is_driver(&self) -> bool {
        &self.0 as &str == DRIVER_EXECUTOR_ID_STR
    }
}

#[derive(Debug)]
pub struct ExecutorInfo {
    id: ExecutorId,
    address: ServiceAddress,
}

struct Executor {
    id: ExecutorId,
    hostname: String,
    pool: ThreadPool,
}

impl Executor {
    pub fn new(id: ExecutorId, hostname: String, cores: usize) -> Self {
        Self {
            id,
            hostname,
            pool: ThreadPool::new(cores),
        }
    }
}

impl Actor for Executor {
    type Context = Context<Self>;
}

impl Handler<TaskStatusEvent> for Executor {
    type Result = ();

    fn handle(&mut self, msg: TaskStatusEvent, ctx: &mut Self::Context) -> Self::Result {
        unimplemented!()
    }
}

struct TaskRunner {}
