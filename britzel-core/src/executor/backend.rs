use actix::{Recipient};
use crate::scheduler::task_manager::TaskStatusEvent;

/// Send updates to cluster scheduler
pub type ExecutorBackend = Recipient<TaskStatusEvent>;