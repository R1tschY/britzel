use crate::broadcast::BroadcastId;
use crate::error::CoreResult;
use crate::executor::ExecutorId;
use crate::rdd::{PartitionId, RddId};
use crate::shuffle::ShuffleId;
use arrow::record_batch::RecordBatch;
use serde::export::Formatter;
use std::collections::HashMap;
use std::fmt;
use std::sync::Arc;

pub trait BlockDataManager {}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum BlockId {
    Shuffle {
        id: ShuffleId,
        map_id: PartitionId,
        reduce_id: PartitionId,
    },
    Broadcast {
        id: BroadcastId,
    },
    Rdd {
        id: RddId,
        part: PartitionId,
    },
}

impl BlockId {
    pub fn for_shuffle(id: ShuffleId, map_id: PartitionId, reduce_id: PartitionId) -> Self {
        BlockId::Shuffle {
            id,
            map_id,
            reduce_id,
        }
    }

    pub fn for_broadcast(id: BroadcastId) -> Self {
        BlockId::Broadcast { id }
    }

    pub fn for_rdd(id: RddId, part: PartitionId) -> Self {
        BlockId::Rdd { id, part }
    }

    pub fn is_shuffle(&self) -> bool {
        if let BlockId::Shuffle { .. } = self {
            true
        } else {
            false
        }
    }
}

impl fmt::Display for BlockId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            BlockId::Shuffle {
                id,
                map_id,
                reduce_id,
            } => f.write_fmt(format_args!(
                "shuffle_{}_{}_{}",
                id.number(),
                map_id,
                reduce_id
            )),
            BlockId::Broadcast { id } => f.write_fmt(format_args!("broadcast_{}", id)),
            BlockId::Rdd { id, part } => f.write_fmt(format_args!("rdd_{}_{}", id, part)),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct StorageLevel {
    memory: bool,
    disk: bool,
    replication: u8,
}

#[derive(Debug, Eq, PartialEq)]
pub struct BlockInfo {
    bytes: u64,
    storage: StorageLevel,
    // TODO: md5?
}

/// interface to provide blocks
pub trait BlockStore {
    fn has_block(&self, block: BlockId) -> bool;
    fn read_block(&self, block: BlockId) -> CoreResult<Option<RecordBatch>>;
    fn all_blocks(&self) -> Vec<BlockId>;
}

/// super generic store for any block
pub struct GenericMemoryBlockStore {
    blocks: HashMap<BlockId, RecordBatch>,
}

impl GenericMemoryBlockStore {
    fn remove_block(&mut self, block: BlockId) {
        self.blocks.remove(&block);
    }

    fn write_block(&mut self, block: BlockId, records: RecordBatch) {
        self.blocks.insert(block.clone(), records);
    }
}

impl BlockStore for GenericMemoryBlockStore {
    fn has_block(&self, block: BlockId) -> bool {
        self.blocks.contains_key(&block)
    }

    fn read_block(&self, block: BlockId) -> CoreResult<Option<RecordBatch>> {
        Ok(self.blocks.get(&block).cloned())
    }

    fn all_blocks(&self) -> Vec<BlockId> {
        self.blocks.keys().cloned().collect()
    }
}

pub trait BlockStoreManager {
    fn register_block(&mut self, store: &dyn BlockStore, block: BlockId);
    fn unregister_block(&mut self, store: &dyn BlockStore, block: BlockId);
    fn register_shuffle_blocks(
        &mut self,
        store: &dyn BlockStore,
        id: ShuffleId,
        map_id: PartitionId,
    );
    fn unregister_shuffle_blocks(
        &mut self,
        store: &dyn BlockStore,
        id: ShuffleId,
        map_id: PartitionId,
    );
}

/// Identifier of a block manager
#[derive(Hash, Eq, PartialEq)]
struct BlockManagerId {
    executor: ExecutorId,
    host: String,
    port: u16,
}

impl BlockManagerId {
    pub fn executor_id(&self) -> &ExecutorId {
        &self.executor
    }

    pub fn host_name(&self) -> &str {
        &self.host
    }

    pub fn host_port(&self) -> u16 {
        self.port
    }
}

struct ShuffleBlockInfo {
    id: ShuffleId,
    map_id: PartitionId,
    bytes: u64,
    storage_type: StorageLevel,
}

pub struct BlockManager {
    shuffle_blocks: ShuffleBlockInfo,
}

impl BlockStoreManager for BlockManager {
    fn register_block(&mut self, store: &dyn BlockStore, block: BlockId) {}

    fn unregister_block(&mut self, store: &dyn BlockStore, block: BlockId) {
        unimplemented!()
    }

    fn register_shuffle_blocks(
        &mut self,
        store: &dyn BlockStore,
        id: ShuffleId,
        map_id: PartitionId,
    ) {
        unimplemented!()
    }

    fn unregister_shuffle_blocks(
        &mut self,
        store: &dyn BlockStore,
        id: ShuffleId,
        map_id: PartitionId,
    ) {
        unimplemented!()
    }
}
