use crate::broadcast::BroadcastId;
use crate::error::CoreResult;
use crate::executor::ExecutorId;
use crate::node::ServiceAddress;
use crate::rdd::PartitionId;
use crate::shuffle::block_manager::{BlockId, BlockInfo, BlockManager, BlockStoreManager};
use crate::shuffle::ShuffleId;
use arrow::record_batch::RecordBatch;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

trait BlockRegistry {
    fn get_map_blocks(&self, id: ShuffleId, reduce_id: PartitionId) -> Vec<ExecutorId>;

    fn register_broadcast(&self, id: BroadcastId, value: RecordBatch) -> CoreResult<()>;
    fn remove_broadcast(&self, id: BroadcastId) -> CoreResult<()>;

    fn register_shuffle_blocks(
        &mut self,
        location: ExecutorId,
        id: ShuffleId,
        map_id: PartitionId,
        info: BlockInfo,
    );
    fn unregister_shuffle_blocks(&mut self, id: ShuffleId);
}

#[derive(Debug)]
struct BlockManagerInfo {
    executor: ExecutorId,
    address: ServiceAddress,
}

/// Registry for all block managers running on driver
pub struct BlockRegistryImpl {
    extern_managers: Mutex<HashMap<ExecutorId, BlockManagerInfo>>,

    /// block manager on driver
    local_manager: Option<Arc<BlockManager>>,
}

impl BlockRegistryImpl {
    pub fn new(local_block_manager: Option<Arc<BlockManager>>) -> Self {
        Self {
            extern_managers: Mutex::new(Default::default()),
            local_manager: local_block_manager,
        }
    }

    pub fn register_block_manager(&self, id: ExecutorId, info: BlockManagerInfo) {
        self.extern_managers.lock().unwrap().insert(id, info);
    }

    pub fn unregister_block_manager(&self, id: ExecutorId) {
        self.extern_managers.lock().unwrap().remove(&id);
    }

    fn push_block(
        &self,
        manager: ServiceAddress,
        id: BlockId,
        block: RecordBatch,
    ) -> CoreResult<()> {
        unimplemented!()
    }

    fn get_extern_addresses(&self) -> Vec<ServiceAddress> {
        let block_managers = self.extern_managers.lock().unwrap();
        block_managers
            .iter()
            .map(|mgr| mgr.1.address.clone())
            .collect()
    }
}

impl BlockRegistry for BlockRegistryImpl {
    fn get_map_blocks(&self, id: ShuffleId, reduce_id: u32) -> Vec<ExecutorId> {
        unimplemented!()
    }

    fn register_broadcast(&self, id: BroadcastId, value: RecordBatch) -> CoreResult<()> {
        let block_id = BlockId::Broadcast { id };

        if let Some(local_mgr) = &self.local_manager {
            local_mgr.push_block(block_id, value);
        }
        // TODO: push to some/all nodes

        Ok(())
    }

    fn remove_broadcast(&self, id: BroadcastId) -> CoreResult<()> {
        let block_id = BlockId::Broadcast { id };

        for address in self.get_extern_addresses() {
            self.remove_block(address, block_id.clone())?;
        }
        if let Some(local_mgr) = &self.local_manager {
            local_mgr.remove_block(block_id)?;
        }
        Ok(())
    }

    fn register_shuffle_blocks(
        &mut self,
        location: ExecutorId,
        id: ShuffleId,
        map_id: u32,
        info: BlockInfo,
    ) {
        unimplemented!()
    }

    fn unregister_shuffle_blocks(&mut self, id: ShuffleId) {
        unimplemented!()
    }
}

/// Client for block manager registry running any executor
pub struct BlockManagerRegistryClient {}
