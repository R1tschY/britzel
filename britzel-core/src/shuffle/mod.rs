use crate::error::CoreResult;
use crate::rdd::PartitionCount;
use crate::rdd::PartitionId;
use crate::scheduler::task::Task;
use crate::scheduler::task_context::TaskContext;
use crate::shuffle::block_manager::BlockStore;
use arrow::array::Array;
use arrow::datatypes::{Schema as ArrowSchema, Schema};
use arrow::record_batch::RecordBatch;
use serde::export::Formatter;
use std::fmt;
use std::sync::Arc;
use uuid::Uuid;

pub mod block_manager;
pub mod block_registry;
pub mod in_memory;

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
pub struct ShuffleId(u32);

impl ShuffleId {
    pub fn new(id: u32) -> Self {
        Self(id)
    }

    pub fn next(&self) -> Self {
        Self::new(self.0 + 1)
    }

    pub fn number(&self) -> u32 {
        self.0
    }
}

impl fmt::Display for ShuffleId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("{}", self.0))
    }
}

pub(crate) struct ShuffleInput {
    pub out_parts: Vec<u32>,
    pub data: RecordBatch,
}

pub(crate) struct ShuffleOutput {
    pub data: RecordBatch,
}

pub(crate) trait ShuffleReader {
    fn read(&self) -> CoreResult<RecordBatch>;
}

pub(crate) trait ShuffleWriter {
    fn write(&self, input: &ShuffleInput) -> CoreResult<()>;
}

/// Interface coordination of shuffle
pub(crate) trait ShuffleManager: BlockStore {
    fn create_shuffle(&mut self, schema: Arc<Schema>, partitions: PartitionCount) -> ShuffleId;
    fn remove_shuffle(&mut self, id: &ShuffleId);

    fn create_reader(
        &self,
        shuffle: &ShuffleId,
        start_partition: PartitionId,
        end_partition: PartitionId,
        task: Arc<Task>,
    ) -> Option<Box<dyn ShuffleReader>>;

    fn create_writer(&self, shuffle: &ShuffleId, task: Arc<Task>)
        -> Option<Box<dyn ShuffleWriter>>;
}

pub(crate) trait ShuffleStorage {
    // TODO
}
