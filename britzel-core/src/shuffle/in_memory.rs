use crate::error::CoreError::InternalError;
use crate::error::{CoreError, CoreResult};
use crate::scheduler::task::Task;
use crate::scheduler::task_context::TaskContext;
use crate::shuffle::block_manager::{BlockId, BlockStore};
use crate::shuffle::{
    ShuffleId, ShuffleInput, ShuffleManager, ShuffleOutput, ShuffleReader, ShuffleWriter,
};
use arrow::array::{Array, ArrayRef};
use arrow::datatypes::Schema;
use arrow::record_batch::RecordBatch;
use britzel_arrow::compute::reorder::reorder;
use britzel_arrow::compute::sort::{argsort, sort};
use britzel_arrow::record_batch::RecordBatchFunctions;
use britzel_arrow::ArrowResult;
use std::collections::HashMap;
use std::ops::{DerefMut, Range};
use std::sync::{Arc, Mutex, MutexGuard};
use superslice::Ext;

struct InMemoryShuffleManager {
    shuffles: HashMap<ShuffleId, Arc<InMemoryShuffle>>,
    last_id: ShuffleId,
}

impl InMemoryShuffleManager {
    pub fn new() -> Self {
        Self {
            shuffles: Default::default(),
            last_id: ShuffleId::new(0),
        }
    }
}

impl ShuffleManager for InMemoryShuffleManager {
    fn create_shuffle(&mut self, schema: Arc<Schema>, partitions: u32) -> ShuffleId {
        self.last_id = self.last_id.next();
        self.shuffles.insert(
            self.last_id,
            Arc::new(InMemoryShuffle {
                id: self.last_id,
                schema,
                storage: Default::default(),
            }),
        );
        self.last_id
    }

    fn remove_shuffle(&mut self, id: &ShuffleId) {
        self.shuffles.remove(id);
    }

    fn create_reader(
        &self,
        shuffle: &ShuffleId,
        start_partition: u32,
        end_partition: u32,
        task: Arc<Task>,
    ) -> Option<Box<dyn ShuffleReader>> {
        self.shuffles.get(shuffle).map(|shuffle| {
            let x: Box<dyn ShuffleReader> = Box::new(InMemoryShuffleReader {
                shuffle: shuffle.clone(),
                start_partition,
                end_partition,
                task,
            });
            x
        })
    }

    fn create_writer(
        &self,
        shuffle: &ShuffleId,
        task: Arc<Task>,
    ) -> Option<Box<dyn ShuffleWriter>> {
        self.shuffles.get(shuffle).map(|shuffle| {
            let x: Box<dyn ShuffleWriter> = Box::new(InMemoryShuffleWriter {
                shuffle: shuffle.clone(),
                task,
            });
            x
        })
    }
}

impl BlockStore for InMemoryShuffleManager {
    fn has_block(&self, block: BlockId) -> bool {
        if let BlockId::Shuffle { .. } = block {
            // only one shuffle manager should exist
            true
        } else {
            false
        }
    }

    fn read_block(&self, block: BlockId) -> CoreResult<Option<RecordBatch>> {
        if let BlockId::Shuffle {
            id,
            map_id,
            reduce_id,
        } = &block
        {
            self.shuffles.get(id).map_or(Ok(None), |shuffle| {
                shuffle
                    .read_output(*map_id)
                    .map(|output| output.map(|o| o.data))
            })
        } else {
            Ok(None)
        }
    }

    fn all_blocks(&self) -> Vec<BlockId> {
        unimplemented!()
    }
}

struct InMemoryShuffle {
    id: ShuffleId,
    schema: Arc<Schema>,
    storage: Mutex<Vec<Option<ShuffleTaskOutput>>>,
}

#[derive(Clone)]
struct ShuffleTaskOutput {
    reduce_part: Arc<Vec<u32>>,
    data: RecordBatch,
}

impl InMemoryShuffle {
    pub fn new(id: ShuffleId, schema: Arc<Schema>, partitions: u32) -> Self {
        Self {
            id,
            schema,
            storage: Mutex::new(vec![None; partitions as usize]),
        }
    }

    fn get_store(&self) -> CoreResult<MutexGuard<Vec<Option<ShuffleTaskOutput>>>> {
        self.storage
            .lock()
            .map_err(|_| InternalError("poisoned shuffle store mutex".to_string()))
    }

    pub fn read_output(&self, partition: u32) -> CoreResult<Option<ShuffleTaskOutput>> {
        let store = self.get_store()?;
        let partition_store = store.get(partition as usize).ok_or_else(|| {
            InternalError(format!(
                "Non-existing shuffle map partition {} for shuffle {}",
                partition, self.id
            ))
        })?;
        Ok(partition_store.clone())
    }

    pub fn write_output(&self, partition: u32, output: ShuffleTaskOutput) -> CoreResult<()> {
        let mut store = self.get_store()?;
        let mut partition_store = store.get_mut(partition as usize).ok_or_else(|| {
            InternalError(format!(
                "Non-existing shuffle map partition {} for shuffle {}",
                partition, self.id
            ))
        })?;
        *partition_store = Some(output);
        Ok(())
    }
}

struct InMemoryShuffleReader {
    shuffle: Arc<InMemoryShuffle>,
    start_partition: u32,
    end_partition: u32,
    task: Arc<Task>,
}

impl ShuffleReader for InMemoryShuffleReader {
    fn read(&self) -> CoreResult<RecordBatch> {
        let output = self
            .shuffle
            .read_output(self.task.partition_id())?
            .ok_or_else(|| {
                CoreError::InternalError(format!(
                    "Missing shuffle output partition {} for shuffle {}",
                    self.task.partition_id(),
                    self.shuffle.id
                ))
            })?;

        // get range of output partition
        let range = if self.start_partition == self.end_partition {
            output.reduce_part.equal_range(&self.start_partition)
        } else {
            Range {
                start: output.reduce_part.lower_bound(&self.start_partition),
                // TODO: do linear search from start?
                end: output.reduce_part.upper_bound(&self.end_partition),
            }
        };

        // slice data
        Ok(output.data.slice(range.start, range.end - range.start))
    }
}

struct InMemoryShuffleWriter {
    shuffle: Arc<InMemoryShuffle>,
    task: Arc<Task>,
}

fn reorder_slice<T: Clone>(slice: &[T], new_order: &[usize]) -> Vec<T> {
    let mut result = Vec::with_capacity(slice.len());
    for new_index in new_order {
        result.push(slice[*new_index].clone());
    }
    result
}

impl ShuffleWriter for InMemoryShuffleWriter {
    fn write(&self, input: &ShuffleInput) -> CoreResult<()> {
        assert_eq!(input.out_parts.len(), input.data.num_rows());

        // sort records by output partition to later read them as slice
        let new_order = argsort(&input.out_parts, |data, a, b| data[a].cmp(&data[b]));
        let record_batch = input.data.try_map_columns(|col| reorder(&new_order, col))?;
        let out_parts = reorder_slice(&input.out_parts, &new_order);

        let output = ShuffleTaskOutput {
            reduce_part: Arc::new(out_parts),
            data: record_batch,
        };
        self.shuffle.write_output(self.task.partition_id(), output)
    }
}
