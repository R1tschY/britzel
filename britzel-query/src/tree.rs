use std::sync::Arc;
use std::ops::Deref;
use std::fmt::{Debug, Formatter, Error};

pub(crate) trait TreeNode {
    type NodeType;

    fn children(&self) -> Vec<&Self::NodeType>;
}

#[derive(Clone)]
pub(crate) struct TreePtr<T: Debug + TreeNode>(Arc<T>);

impl<T: Debug + TreeNode> Deref for TreePtr<T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.0.deref()
    }
}

impl<T: Debug + TreeNode> From<Arc<T>> for TreePtr<T> {
    fn from(r: Arc<T>) -> Self {
        TreePtr(r.clone())
    }
}

impl<T: Debug + TreeNode> From<T> for TreePtr<T> {
    fn from(r: T) -> Self {
        TreePtr(Arc::new(r))
    }
}

impl<T: Debug + TreeNode> Debug for TreePtr<T> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> Result<(), Error> {
        self.0.fmt(fmt)
    }
}