use std::fmt::{Debug, Error, Formatter};
use std::ops::Deref;
use std::sync::Arc;

use arrow::array::{Array, ArrayRef};
use arrow::compute::kernels::arithmetic::add;
use arrow::compute::kernels::boolean::{and, or};
use arrow::compute::kernels::cast::cast;
use arrow::compute::kernels::comparison::{eq, gt, gt_eq, lt, lt_eq, neq};
use arrow::datatypes::{BooleanType, Float32Type, Float64Type, Int16Type, Int32Type, Int64Type, Int8Type, UInt16Type, UInt32Type, UInt64Type, UInt8Type};
use arrow::record_batch::RecordBatch;

use britzel_arrow::array::{full_primitive_array, full_string_array};

use crate::expr::error::{AnalysisResult, BuildAnalysisError};
use crate::expr::error::BuildAnalysisResult;
use crate::schema::{AnyData, DataType};
use crate::tree::{TreePtr, TreeNode};

pub(crate) type ExprPtr = TreePtr<ExprNode>;

#[derive(Copy, Clone, Debug)]
pub(crate) enum UnaryOpType {
    Not,
    BitNot,
}

#[derive(Copy, Clone, Debug)]
pub(crate) enum BinOpType {
    Add,
    Sub,
    Mul,
    Div,
    Eq,
    Ne,
}

impl BinOpType {
    pub fn has_boolean_result(&self) -> bool {
        match self {
            BinOpType::Eq | BinOpType::Ne => true,
            _ => false
        }
    }
}

#[derive(Clone, Debug)]
pub enum Value {
    NonNull(AnyData),
    Null(DataType),
}

impl Value {
    pub fn data_type(&self) -> DataType {
        match self {
            Value::NonNull(data) => data.data_type(),
            Value::Null(dtype) => dtype.clone()
        }
    }
}

#[derive(Clone, Debug)]
pub(crate) enum ExprNode {
    BinOp(BinOpType, ExprPtr, ExprPtr),
    UnaryOp(UnaryOpType, ExprPtr),

    Cast(DataType, ExprPtr),

    Literal(Value),

    Alias(String, ExprPtr),
    Column { index: usize, data_type: DataType, nullable: bool },

    AttrRef { name: String, data_type: DataType, nullable: bool },
    UnresolvedAttr { name_parts: Vec<String> },
    Call { name: String, args: Vec<ExprPtr>, ret: DataType, nullable: bool, deterministic: bool },
}

impl ExprNode {
    pub fn is_physical(&self) -> bool {
        use self::ExprNode::*;

        match self {
            AttrRef { .. } | UnresolvedAttr { .. } => false,
            _ => self.children().iter().all(|expr| expr.is_physical()),
        }
    }

    pub fn attributes(&self) -> Vec<String> {
        let mut attrs: Vec<String> = vec![];
        self.collect_attributes(&mut attrs);
        attrs
    }

    fn collect_attributes(&self, attributes: &mut Vec<String>) {
        use self::ExprNode::*;

        if let AttrRef { name, .. } = self {
            attributes.push(name.clone());
        } else {
            for child in self.children() {
                child.collect_attributes(attributes);
            }
        }
    }

    pub fn foldable(&self) -> bool {
        use self::ExprNode::*;

        match self {
            Cast(_, expr) => expr.foldable(),
            UnaryOp(_, expr) => expr.foldable(),
            Alias(_, expr) => expr.foldable(),
            BinOp(_, lhs, rhs) => lhs.foldable() & rhs.foldable(),
            Call { args, deterministic, .. } =>
                deterministic & args.iter().all(|arg| arg.foldable()),
            _ => false
        }
    }

    /// Data type of result
    pub fn data_type(&self) -> Option<DataType> {
        use self::ExprNode::*;

        match self {
            BinOp(op, lhs, rhs) if op.has_boolean_result() => Some(DataType::Bool),
            BinOp(_, lhs, rhs) => {
                if let Some(ldt) = lhs.data_type() {
                    if let Some(rdt) = rhs.data_type() {
                        return DataType::common_type(&ldt, &rdt);
                    }
                }
                None
            }
            UnaryOp(UnaryOpType::Not, expr) => Some(DataType::Bool),
            UnaryOp(UnaryOpType::BitNot, expr) => Some(DataType::Bool),
            Cast(data_type, _) => Some(data_type.clone()),

            Literal(lit) => Some(lit.data_type()),

            Alias(_, expr) => expr.data_type(),
            Column { data_type, .. } => Some(data_type.clone()),
            AttrRef { data_type, .. } => Some(data_type.clone()),
            UnresolvedAttr { name_parts } => None,
            Call { ret, .. } => Some(ret.clone()),
        }
    }

    pub fn nullable(&self) -> bool {
        use self::ExprNode::*;

        match self {
            BinOp(_, lhs, rhs) => lhs.nullable() | rhs.nullable(),
            UnaryOp(_, expr) => expr.nullable(),
            Cast(_, expr) => expr.nullable(),
            Alias(_, expr) => expr.nullable(),

            Literal(value) => if let Value::Null(_) = value { true } else { false },

            Column { nullable, .. } => *nullable,
            AttrRef { nullable, .. } => *nullable,
            Call { nullable, .. } => *nullable,

            UnresolvedAttr { name_parts } =>
                panic!("cannot analyse nullable of unresolved attribute"),
        }
    }
}

impl TreeNode for ExprNode {
    type NodeType = ExprNode;

    fn children(&self) -> Vec<&ExprNode> {
        use self::ExprNode::*;

        match self {
            BinOp(_, lhs, rhs) => vec![lhs.deref(), rhs.deref()],
            UnaryOp(_, expr) => vec![expr.deref()],

            Cast(_type, expr) => vec![expr.deref()],
            Call { args, .. } => args.iter().map(|arg| arg.deref()).collect(),
            Alias(_, expr) => vec![expr.deref()],

            Column { .. } | AttrRef { .. } | UnresolvedAttr { .. } | Literal(_) => vec![],
        }
    }
}
//
//struct MemoryStore {
//    pub schema: Schema,
//}
//
//trait Catalog {
//    fn check(&self, name: &str) -> BuildAnalysisResult<()>;
//    fn data_type(&self, name: &str) -> Option<DataType>;
//    fn column_data_type(&self, name: &str) -> Option<DataType>;
//    fn data(&self, name: &str) -> BuildAnalysisResult<ArrayRef>;
//}
//
//impl Catalog for MemoryStore {
//    fn check(&self, name: &str) -> BuildAnalysisResult<()> {
//        if self.schema.column_with_name(name).is_none() {
//            Err(BuildAnalysisError::NoSuchAttribute(name.into()))
//        } else {
//            Ok(())
//        }
//    }
//
//    fn data_type(&self, name: &str) -> Option<DataType> {
//        match self.schema.column_with_name(name) {
//            Some((index, field)) => Some(field.data_type().into()),
//            None => None,
//        }
//    }
//
//    fn column_data_type(&self, name: &str) -> Option<DataType> {
//
//    }
//}

pub(crate) trait ExprExec {
    fn exec(&self, store: &RecordBatch) -> AnalysisResult<ArrayRef>;
}


impl ExprExec for ExprNode {
    fn exec(&self, data: &RecordBatch) -> AnalysisResult<ArrayRef> {
        use self::ExprNode::*;
        use self::Value::*;

        Ok(match self {
            Literal(value) => match value {
                NonNull(value) => full_array(value, data.num_rows()),
                Null(data_type) => unimplemented!(),
            }
            BinOp(op, lhs, rhs) => {
                use self::BinOpType::*;

                panic!(lhs.data_type().unwrap() == rhs.data_type().unwrap());

                match (op, lhs.exec(data)?, rhs.exec(data)?) {
//                    (Add, l, r) => add(&l, &r)?,
                    _ => unimplemented!()
                }
            }
            Cast(data_type, body) => cast(&body.exec(data)?, &data_type.into())?,
            _ if !self.is_physical() => panic!("non physical expr cannot be executed"),
            _ => unimplemented!()
        })
    }
}

fn full_array(value: &AnyData, rows: usize) -> Arc<dyn Array> {
   match value {
        AnyData::Bool(v) => Arc::new(full_primitive_array::<BooleanType>(v, rows)),
        AnyData::I8(v) => Arc::new(full_primitive_array::<Int8Type>(v, rows)),
        AnyData::I16(v) => Arc::new(full_primitive_array::<Int16Type>(v, rows)),
        AnyData::I32(v) => Arc::new(full_primitive_array::<Int32Type>(v, rows)),
        AnyData::I64(v) => Arc::new(full_primitive_array::<Int64Type>(v, rows)),
        AnyData::U8(v) => Arc::new(full_primitive_array::<UInt8Type>(v, rows)),
        AnyData::U16(v) => Arc::new(full_primitive_array::<UInt16Type>(v, rows)),
        AnyData::U32(v) => Arc::new(full_primitive_array::<UInt32Type>(v, rows)),
        AnyData::U64(v) => Arc::new(full_primitive_array::<UInt64Type>(v, rows)),
        AnyData::F32(v) => Arc::new(full_primitive_array::<Float32Type>(v, rows)),
        AnyData::F64(v) => Arc::new(full_primitive_array::<Float64Type>(v, rows)),
        AnyData::Str(v) => Arc::new(full_string_array(v, rows)),
        _ => unimplemented!(),
    }
}
