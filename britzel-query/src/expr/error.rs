
#[derive(Debug)]
pub enum BuildAnalysisError {
    NoSuchAttribute(String),
    UnresolvedAttribute(Vec<String>),
    TypeError(String),
}

pub type BuildAnalysisResult<T> = Result<T, BuildAnalysisError>;

quick_error! {
    #[derive(Debug)]
    pub enum AnalysisError {
        Arrow(err: arrow::error::ArrowError) {
            from()
            description(err.description())
            display("Arrow error: {:?}", err)
        }
        Unimplemented(message: String) {
            display("Not yet supported feature: {}", message)
            description(message)
        }
    }
}

pub type AnalysisResult<T> = Result<T, AnalysisError>;