/*
use crate::queries::schema::AnyData;
use crate::queries::schema::DataType;
use crate::queries::expr::Expression;
use smallvec::SmallVec;
use crate::queries::expr::CodeGenCtx;
use crate::queries::expr::ExprCode;

pub enum Literal {
    NonNull(AnyData),
    Null(DataType)
}


impl Literal {
    pub fn new(literal: AnyData) -> Literal {
        if literal != AnyData::Null {
            Literal::NonNull(literal)
        } else {
            Literal::Null(DataType::Null)
        }
    }

    pub fn new_null(dtype: DataType) -> Literal {
        Literal::Null(dtype)
    }
}

impl Expression for Literal {
    fn children<'a>(&'a self) -> SmallVec<[&'a dyn Expression; 3]> {
        smallvec![]
    }

    fn nullable(&self) -> bool {
        match self {
            Literal::Null(_) => true,
            _ => false
        }
    }

    fn deterministic(&self) -> bool {
        true
    }

    fn dtype(&self) -> DataType {
        match self {
            Literal::NonNull(lit) => lit.data_type(),
            Literal::Null(dtype) => *dtype
        }
    }

    fn foldable(&self) -> bool {
        true
    }

    fn fold(&self) -> Option<AnyData> {
        match self {
            Literal::NonNull(lit) => Some(lit.clone()),
            Literal::Null(_) => None
        }
    }

    fn gen_code(&self, ctx: CodeGenCtx, ev: ExprCode) -> ExprCode {
        unimplemented!()
    }

    fn sql(&self) -> String {
        use crate::queries::schema::AnyData::*;

        match self {
            Literal::Null(DataType::Null) => String::from("NULL"),
            Literal::Null(dtype) => format!("CAST(NULL AS {})", dtype.sql()),
            Literal::NonNull(lit) => match lit {
                U8(v) => format!("{}UY", v),
                I8(v) => format!("{}Y", v),
                U16(v) => format!("{}US", v),
                I16(v) => format!("{}S", v),
                U32(v) => format!("{}U", v),
                I32(v) => format!("{}", v),
                U64(v) => format!("{}UL", v),
                I64(v) => format!("{}L", v),
                F32(v) => match v {
                    _ if v.is_infinite() && v.is_sign_positive() =>
                        String::from("CAST('Infinity' AS FLOAT)"),
                    _ if v.is_infinite() && v.is_sign_negative() =>
                        String::from("CAST('-Infinity' AS FLOAT)"),
                    _ if v.is_nan() => String::from("CAST('NaN' AS FLOAT)"),
                    _ => format!("{}f", v),
                },
                F64(v) => match v {
                    _ if v.is_infinite() && v.is_sign_positive() =>
                        String::from("CAST('Infinity' AS DOUBLE)"),
                    _ if v.is_infinite() && v.is_sign_negative() =>
                        String::from("CAST('-Infinity' AS DOUBLE)"),
                    _ if v.is_nan() => String::from("CAST('NaN' AS DOUBLE)"),
                    _ => format!("{}d", v),
                },
                Bool(v) => if *v { "TRUE".to_string() } else { "FALSE".to_string() },
                Str(v) => format!("'{}'", v.replace("\\", "\\\\").replace("'", "\\'")),
                Null => unreachable!()
            }
        }
    }
}*/
