/*
use crate::queries::expr::Expression;
use smallvec::SmallVec;
use crate::queries::expr::DataType;
use crate::queries::expr::AnyData;
use crate::queries::expr::CodeGenCtx;
use crate::queries::expr::ExprCode;
use uuid::Uuid;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::SeqCst;
use crate::queries::sql::quote_identifier;


pub struct ExprId{
    id: usize,
    process_uuid: Uuid
}

lazy_static! {
    static ref PROCESS_UUID: Uuid = Uuid::new_v4();
}

static LAST_EXPR_ID: AtomicUsize = AtomicUsize::new(0);


impl ExprId {
    pub fn new() -> Self {
        let id = LAST_EXPR_ID.fetch_add(1, SeqCst);
        ExprId { id, process_uuid: *PROCESS_UUID }
    }
}

/// Attribute

pub trait Attribute {
    fn name(&self) -> String;
}

pub struct UnresolvedAttribute {
    name_parts: Vec<String>
}

impl<'a> Attribute for UnresolvedAttribute {
    fn name(&self) -> String {
        self.name_parts.iter()
            .map(|name| if name.contains(".") { format!("`{}`", name) } else { name.clone() })
            .collect::<Vec<_>>()
            .join(".")
    }
}


pub struct AttributeReference {
    pub name: String,
    pub dtype: DataType,
    pub nullable: bool,
    // pub metadata: ...
    pub expr_id: ExprId,
    pub qualifier: Option<String>
}

impl AttributeReference {
    pub fn new(name: String, dtype: DataType, nullable: bool) -> AttributeReference {
        AttributeReference {
            name, dtype, nullable,
            expr_id: ExprId::new(),
            qualifier: None
        }
    }
}

impl Attribute for AttributeReference {
    fn name(&self) -> String { self.name.clone() }
}

impl Expression for AttributeReference {
    fn children(&self) -> SmallVec<[&dyn Expression; 3]> {
        smallvec![]
    }

    fn nullable(&self) -> bool {
        self.nullable
    }

    fn deterministic(&self) -> bool {
        true
    }

    fn dtype(&self) -> DataType {
        self.dtype
    }

    fn foldable(&self) -> bool {
        false
    }

    fn fold(&self) -> Option<AnyData> {
        None
    }

    fn gen_code(&self, ctx: CodeGenCtx, ev: ExprCode) -> ExprCode {
        unimplemented!()
    }

    fn sql(&self) -> String {
        match self.qualifier {
            None => quote_identifier(&self.name),
            Some(ref qualifier) => format!("{}.{}", qualifier, quote_identifier(&self.name)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::queries::expr::attribute::AttributeReference;
    use std::sync::Arc;

//    #[test]
//    fn sql_expr() {
//        let expr: Arc<Expression> =
//            Arc::new(AttributeReference::new(String::from("a"), DataType::I32, false));
//
//        assert_eq!(expr.sql(), "`a`");
//
//        let expr: Arc<Expression> =
//            Arc::new(AttributeReference::new(String::from("a`"), DataType::I32, false));
//
//        assert_eq!(expr.sql(), "`a```");
//    }
}*/
