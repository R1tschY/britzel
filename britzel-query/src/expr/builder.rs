use crate::expr::node::{ExprPtr, ExprNode, Value, BinOpType};
use crate::schema::{AnyData, DataType};
use std::sync::Arc;
use std::fmt::{Debug, Formatter, Error};

#[derive(Clone, Debug)]
pub struct ExprBuilder(ExprPtr);

impl From<ExprNode> for ExprBuilder {
    fn from(r: ExprNode) -> Self {
        ExprBuilder(r.into())
    }
}

impl From<&ExprBuilder> for ExprPtr {
    fn from(r: &ExprBuilder) -> Self {
        r.0.clone()
    }
}

pub fn lit(value: Option<AnyData>) -> ExprBuilder {
    ExprNode::Literal(match value {
        None => Value::Null(DataType::Null),
        Some(value) => Value::NonNull(value),
    }).into()
}

pub fn col(name: String) -> ExprBuilder {
    ExprNode::UnresolvedAttr { name_parts: vec![name] }.into()
}

impl ExprBuilder {
    pub fn alias(&self, name: &str) -> ExprBuilder {
        ExprNode::Alias(name.to_owned(), self.into()).into()
    }

    pub fn eq(&self, expr: &ExprBuilder) -> ExprBuilder {
        ExprNode::BinOp(BinOpType::Eq, self.into(), expr.into()).into()
    }

    pub fn ne(&self, expr: &ExprBuilder) -> ExprBuilder {
        ExprNode::BinOp(BinOpType::Ne, self.into(), expr.into()).into()
    }

    pub fn cast(&self, data_type: DataType) -> ExprBuilder {
        ExprNode::Cast(data_type, self.into()).into()
    }
}