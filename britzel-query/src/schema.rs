use std::f32;
use std::f64;
use std::cmp::max;
use std::mem;

struct StructType {
    fields: Vec<StructField>,
}

struct StructField {
    name: String,
    dtype: DataType,
    nullable: bool
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Debug)]
pub enum DataType {
    I8 = 0,
    U8 = 1,
    I16 = 2,
    U16 = 3,
    I32 = 4,
    U32 = 5,
    I64 = 6,
    U64 = 7,
    F32 = 8,
    F64 = 9,
    Bool = 10,
    Str = 11,
    Null = 12,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum DataMetaType {
    Integer, Float, Other
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum ImplicitConversionType {
    No, Safe, C
}

impl DataType {
    pub fn sql(&self) -> &'static str {
        use crate::schema::DataType::*;

        match self {
            U8 => "TINYINT UNSIGNED",
            I8 => "TINYINT",
            U16 => "SMALLINT UNSIGNED",
            I16 => "SMALLINT",
            U32 => "INT UNSIGNED",
            I32 => "INT",
            U64 => "BIGINT UNSIGNED",
            I64 => "BIGINT",
            F32 => "FLOAT",
            F64 => "DOUBLE",
            Bool => "BOOLEAN",
            Str => "TEXT CHARACTER SET utf8",
            Null => "NULL",
        }
    }

    pub fn upgrade(&self) -> DataType {
        use crate::schema::DataType::*;

        match self {
            U8 => U16,
            I8 => I16,
            U16 => U32,
            I16 => I32,
            U32 => U64,
            I32 => I64,
            U64 => U64,
            I64 => I64,
            F32 => F32,
            F64 => F64,
            Bool => Bool,
            Str => Str,
            Null => Null,
        }
    }

    pub fn bytes(&self) -> usize {
        use crate::schema::DataType::*;

        match self {
            U8 | I8 => 1,
            U16 | I16 => 2,
            U32 | I32 => 4,
            U64 | I64 => 8,
            F32 => 4,
            F64 => 8,
            Bool => 1,
            Str => mem::size_of::<String>(),
            Null => 0,
        }
    }

    pub fn metatype(&self) -> DataMetaType {
        use crate::schema::DataType::*;
        use crate::schema::DataMetaType::*;

        match self {
            U8 | U16 | U32 | U64 => Integer,
            I8 | I16 | I32 | I64 => Integer,
            F32 | F64 => Float,
            Bool | Str | Null => Other,
        }
    }

    fn can_safe_convert_to(&self, other: &DataType) -> bool {
        use crate::schema::DataMetaType::*;

        if self == other {
            return true;
        }

        match (self.metatype(), other.metatype()) {
            (Integer, Integer) => other.bytes() > self.bytes(),
            (Float, Float) => other.bytes() > self.bytes(),
            _ => false
        }
    }

    pub fn safe_common_type(lhs: &DataType, rhs: &DataType) -> Option<DataType> {
        use crate::schema::DataType::*;
        use crate::schema::DataMetaType::*;

        if lhs.can_safe_convert_to(rhs) {
            return Some(rhs.clone());
        }
        if rhs.can_safe_convert_to(lhs) {
            return Some(lhs.clone());
        }

        // TODO: upgrade

        None
    }

    pub fn c_common_type(lhs: &DataType, rhs: &DataType) -> Option<DataType> {
        use crate::schema::DataType::*;

        let biggest = max(lhs, rhs);
        if biggest <= &I32 {
            Some(I32)
        } else if biggest <= &F64 {
            Some(biggest.clone())
        } else if lhs == rhs {
            Some(lhs.clone())
        } else {
            None
        }
    }

    pub fn common_type(lhs: &DataType, rhs: &DataType) -> Option<DataType> {
        use crate::schema::ImplicitConversionType::*;
        let kind = ImplicitConversionType::No;

        match kind {
            No => if lhs == rhs {
                Some(lhs.clone())
            } else {
                None
            },

            Safe => Self::safe_common_type(lhs, rhs),
            C => Self::c_common_type(lhs, rhs),
        }
    }
}

impl From<&arrow::datatypes::DataType> for DataType {
    fn from(dtype: &arrow::datatypes::DataType) -> Self {
        use arrow::datatypes::DataType::*;

        match dtype {
            Boolean => DataType::Bool,
            Int8 => DataType::I8,
            Int16 => DataType::I16,
            Int32 => DataType::I32,
            Int64 => DataType::I64,
            UInt8 => DataType::U8,
            UInt16 => DataType::U16,
            UInt32 => DataType::U32,
            UInt64 => DataType::U64,
            Float32 => DataType::F32,
            Float64 => DataType::F64,
            Utf8 => DataType::Str,
            _ => unimplemented!(),
        }
    }
}

impl From<&DataType> for arrow::datatypes::DataType {
    fn from(dtype: &DataType) -> Self {
        use arrow::datatypes::DataType::*;

        match dtype {
            DataType::Bool => Boolean,
            DataType::I8 => Int8,
            DataType::I16 => Int16,
            DataType::I32 => Int32,
            DataType::I64 => Int64,
            DataType::U8 => UInt8,
            DataType::U16 => UInt16,
            DataType::U32 => UInt32,
            DataType::U64 => UInt64,
            DataType::F32 => Float32,
            DataType::F64 => Float64,
            DataType::Str => Utf8,
            _ => unimplemented!(),
        }
    }
}


#[derive(PartialEq, Clone, Debug)]
pub enum AnyData {
    U8(u8), I8(i8),
    U16(u16), I16(i16),
    U32(u32), I32(i32),
    U64(u64), I64(i64),
    F32(f32), F64(f64),
    Bool(bool),
    Str(String)
}

impl AnyData {
    pub fn data_type(&self) -> DataType {
        use crate::schema::AnyData::*;

        match self {
            &U8(_) => DataType::U8,
            &I8(_) => DataType::I8,
            &U16(_) => DataType::U16,
            &I16(_) => DataType::I16,
            &U32(_) => DataType::U32,
            &I32(_) => DataType::I32,
            &U64(_) => DataType::U64,
            &I64(_) => DataType::I64,
            &F32(_) => DataType::F32,
            &F64(_) => DataType::F64,
            &Bool(_) => DataType::Bool,
            &Str(_) => DataType::Str,
        }
    }

//    pub fn as_trait<T>(&self) -> &T {
//        use ::queries::expr::AnyData::*;
//
//        match self {
//            &U8(x) => &x as &T,
//            &I8(x) => &x as &T,
//            &U16(x) => &x as &T,
//            &I16(x) => &x as &T,
//            &U32(x) => &x as &T,
//            &I32(x) => &x as &T,
//            &U64(x) => &x as &T,
//            &I64(x) => &x as &T,
//            &F32(x) => &x as &T,
//            &F64(x) => &x as &T,
//            &Str(x) => &x as &T,
//        }
//    }
}
