use std::sync::Arc;
use crate::tree::{TreeNode, TreePtr};
use crate::expr::node::{ExprPtr, ExprExec};
use std::ops::Deref;
use crate::schema::DataType;
use arrow::record_batch::RecordBatch;
use crate::expr::error::AnalysisResult;
use arrow::array::ArrayRef;
use arrow::datatypes::Schema;
use std::fmt::Debug;

// Plans

/*
pub trait QueryPlan: TreeNode {

}


pub trait LogicalPlan : QueryPlan {

}

pub trait PhysicalPlan : Iterator {

}

pub trait CodegenEntry {

}
*/


// Plans

#[derive(Debug)]
pub(crate) enum LogicalPlanNode {
    Project(Vec<ExprPtr>, LogicalPlan),
    Filter(ExprPtr, LogicalPlan),

    FileScan(),
    TableScan(),
    MemoryScan(),
}

impl TreeNode for LogicalPlanNode {
    type NodeType = LogicalPlanNode;

    fn children(&self) -> Vec<&Self::NodeType> {
        use self::LogicalPlanNode::*;

        match self {
            Project(_, child) => vec![child.deref()],
            Filter(_, child) => vec![child.deref()],
            _ => unimplemented!()
        }
    }
}

pub(crate) type LogicalPlan = TreePtr<LogicalPlanNode>;

//

pub(crate) trait Relation: Debug {
    // fn session() -> Session;
    fn schema(&self) -> DataType;

    // unhandledFilters
    // sizeInBytes
}

#[derive(Debug)]
pub(crate) enum PhysicalPlanNode {
    Project(Vec<ExprPtr>, PhysicalPlan),
    Filter(ExprPtr, PhysicalPlan),

    Scan(Arc<dyn Relation>),
}

impl PhysicalPlanNode {
    pub fn schema(&self) -> Arc<Schema> {
        use self::PhysicalPlanNode::*;

        match self {
            _ => unimplemented!()
        }
    }
}

impl TreeNode for PhysicalPlanNode {
    type NodeType = PhysicalPlanNode;

    fn children(&self) -> Vec<&Self::NodeType> {
        use self::PhysicalPlanNode::*;

        match self {
            Project(_, child) => vec![child.deref()],
            Filter(_, child) => vec![child.deref()],
            Scan(_) => vec![],
            _ => unimplemented!()
        }
    }
}

pub(crate) type PhysicalPlan = TreePtr<PhysicalPlanNode>;

pub(crate) trait PlanExec {
    fn exec(&self, rb: &RecordBatch) -> AnalysisResult<RecordBatch>;
}

impl PlanExec for PhysicalPlanNode {
    fn exec(&self, rb: &RecordBatch) -> AnalysisResult<RecordBatch> {
        use self::PhysicalPlanNode::*;

        let columns: Vec<ArrayRef> = match self {
            Project(exprs, child) =>
                exprs.iter().map(|expr| expr.exec(rb)).collect::<AnalysisResult<Vec<ArrayRef>>>()?,
            _ => unimplemented!()
        };

        Ok(RecordBatch::try_new(self.schema(), columns)?)
    }
}

/*
pub struct Project {
    exprs: Vec<Arc<Expression>>
}

pub struct Filter {

}

pub struct FileSource {
    // inner: Box<FileSourceInterface>
}

*/


