use std::fmt::{Formatter, Display, Error};

trait DisplaySql {
    fn display_sql(&self, f: &mut Formatter<'_>) -> Result<(), Error>;
}

pub fn quote_identifier(identifier: &str) -> String {
    format!("`{}`", identifier.replace("`", "``"))
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_quote_identifier() {
        assert_eq!(quote_identifier("a"), "`a`");
        assert_eq!(quote_identifier("a`"), "`a```");
        assert_eq!(quote_identifier("a``b"), "`a````b`");
    }
}