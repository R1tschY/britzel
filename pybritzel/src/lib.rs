extern crate pyo3;

use britzel_query::expr::builder::{ExprBuilder, lit};

use pyo3::prelude::*;
use pyo3::{wrap_pyfunction, PyResult, Python, PyObjectProtocol};
use pyo3::basic::CompareOp;
use britzel_query::schema::AnyData;


macro_rules! lit_fn {
    ($name:ident, $t:ident, $e:ident) => {
        #[pyfunction]
        fn $name(value: $t) -> PyResult<PyExprBuilder> {
            Ok(lit(Some(AnyData::$e(value))).into())
        }
    };
}

lit_fn!(lit_i8, i8, I8);
lit_fn!(lit_u8, u8, U8);

lit_fn!(lit_i16, i16, I16);
lit_fn!(lit_u16, u16, U16);

lit_fn!(lit_i32, i32, I32);
lit_fn!(lit_u32, u32, U32);

lit_fn!(lit_i64, i64, I64);
lit_fn!(lit_u64, u64, U64);

lit_fn!(lit_f32, f32, F32);
lit_fn!(lit_f64, f64, F64);


#[pyfunction]
fn lit_null() -> PyResult<PyExprBuilder> {
    Ok(lit(None).into())
}


#[pyclass]
struct PyExprBuilder {
    expr: ExprBuilder,
}
/*
#[pyfunction(name = "lit")]
fn pylit(value: &PyAnyData) -> PyResult<PyExprBuilder> {
    Ok(lit(value.into()).into())
}*/

#[pymethods]
impl PyExprBuilder {

}

#[pyproto]
impl<'p> PyObjectProtocol<'p> for PyExprBuilder {
    fn __repr__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self.expr))
    }

    fn __richcmp__(&self, other: &PyExprBuilder, op: CompareOp) -> PyResult<PyExprBuilder> {
        match op {
            CompareOp::Eq => Ok(self.expr.eq(&other.expr).into()),
            _ => unimplemented!()
        }
    }
}

impl From<ExprBuilder> for PyExprBuilder {
    fn from(expr: ExprBuilder) -> Self {
        PyExprBuilder { expr }
    }
}

#[pymodule]
fn _britzel(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyExprBuilder>()?;

    m.add_wrapped(wrap_pyfunction!(lit_null)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_u8)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_u16)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_u32)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_u64)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_i8)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_i16)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_i32)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_i64)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_f32)).unwrap();
    m.add_wrapped(wrap_pyfunction!(lit_f64)).unwrap();

    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
