from setuptools import setup
from setuptools_rust import Binding, RustExtension, Strip

setup(
    name="britzel",
    version="0.1",
    rust_extensions=[RustExtension(
        "britzel._britzel", binding=Binding.PyO3, strip=Strip.Debug)],
    packages=["britzel"],
    zip_safe=False,
)